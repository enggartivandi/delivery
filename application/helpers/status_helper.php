<?php

class StatusOrder
{
  private static $messageMapping = array(
    'UNPROCESSED'   => 'checkout berhasil silahkan lakukan proses pembayaran',
    'AUTHENTICATED' => 'proses authorisasi 3D secure berhasil',
    'CHECK'         => 'proses validasi berhasil',
    'CAPTURE'       => 'proses pembelian secara instant berhasil',
    'AUTH'          => 'proses pembelian sementara berhasil',
    'SALES'         => 'pembelian berhasil dilakukan',
    'VOID'          => 'pembelian berhasil dibatalkan',
    'RETURN'        => 'proses pengembalian pembayaran',
    'RETURNX'       => '',
    'SAUTH'         => '',
    'REQSUCCESS'    => 'menunggu konfirmasi pembayaran',
    'PAYFAIL'       => 'proses pembayaran gagal dilakukan',
    'EXPIRED'       => 'batas waktu pembayaran telah usai',
    'PAYSUCCESS'    => 'proses pembayaran berhasil',
    'CANCEL'        => 'proses pembelian dibatalkan',
    'PAYSTART'      => 'prosedur pembayaran dijalankan',
    'REQCANCEL'     => '',
    'REQCHANGE'     => '',
    'REGISTER'      => '',
    'REQAUTH'       => '',
    'REQCAPTURE'    => '',
    'PAYCANCEL'     => '',
    'TRADING'       => 'menunggu proses transfer pembelian',
    'TRANSFERED'    => 'pembelian berhasil dilakukan'
  );

  public static function getStatusMessage($message)
  {
    if (array_key_exists($message, self::$messageMapping)) {
      return self::$messageMapping[$message];
    }
  }
}
