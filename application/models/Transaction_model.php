<?php

class Transaction_model extends CI_Model
{
  public function add_transaction($sale_id, $access_id, $access_pass, $pay_type)
  {
    $data = array(
      'sale_id' => $sale_id,
      'access_id' => $access_id,
      'access_pass' => $access_pass,
      'pay_type'  => $pay_type
    );

    $this->db->insert('transaction', $data);
    $insert_id = $this->db->insert_id();
    return $insert_id;
  }

  public function save_response($transaction_id, $response)
  {
    $data = array(
      'transaction_id' => $transaction_id,
      'data' => $response
    );

    $this->db->insert('transaction_details', $data);
  }

  public function get_data_customer($id)
  {
    $this->db->select(['sale_id', 'total_amount', 'receiver_name', 'receiver_mobile', 'user_fullname', 'user_phone', 'user_email', 'delivery_time_from', 'delivery_time_to']);
    $this->db->join('user_location', 'sale.user_id = user_location.user_id', 'left');
    $this->db->join('registers', 'sale.user_id = registers.user_id', 'left');
    $query = $this->db->get_where('sale', array('sale_id' => $id));

    return $query->row();
  }

  public function get_transaction($id)
  {
    $data = $this->db->get_where('transaction', array('sale_id' => $id));
    return $data->row();
  }

  public function get_transaction_info($id)
  {
    $this->db->select('*');
    $this->db->join('sale', 'transaction_details.transaction_id = sale.sale_id');
    $this->db->join('transaction', 'transaction_details.transaction_id = transaction.sale_id', 'right');
    $data = $this->db->get_where('transaction_details', array('transaction_id' => $id));
    return $data->row();
  }
}
