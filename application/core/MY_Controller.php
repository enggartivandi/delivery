<?php
Class MY_Controller Extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->lang->load('ps', 'japan');
//        if($this->session->userdata('language') == "japan"){
//            $this->lang->load('ps', 'japan');
//        }else{
//            $this->lang->load('ps', 'english');
//        }
        if($this->input->get('lang')){
            $this->session->set_userdata(array("language"=>$this->input->get('lang')));
            $this->lang->load('ps', $this->input->get('lang'));
        }
    }
    
}
?>
