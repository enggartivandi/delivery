-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: mysql5012.xserver.jp
-- Generation Time: Jun 18, 2020 at 02:57 PM
-- Server version: 5.7.27
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appkey03_delivery001`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `slug` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `parent` int(50) NOT NULL,
  `leval` int(50) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `image` varchar(200) NOT NULL,
  `status` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `slug`, `parent`, `leval`, `description`, `image`, `status`) VALUES
(46, 'ドリンク', 'ドリンク', 0, 2, '', '03-drink.jpg', '1'),
(47, 'デサート', 'デサート', 0, 1, '', '03-dessert.jpg', '1'),
(52, 'ピザ', 'ピザ', 0, 2, '', '03-pizza.jpg', '1'),
(53, 'パスタ', 'パスタ', 0, 3, '', '03-pasta.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `closing_hours`
--

CREATE TABLE IF NOT EXISTS `closing_hours` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `closing_hours`
--

INSERT INTO `closing_hours` (`id`, `date`, `from_time`, `to_time`) VALUES
(1, '2017-02-06', '10:30:00', '16:00:00'),
(3, '2017-11-15', '09:00:00', '21:00:00'),
(4, '2017-04-10', '09:00:00', '19:00:00'),
(5, '2017-05-05', '09:00:00', '21:00:00'),
(6, '2017-12-12', '09:30:00', '19:00:00'),
(7, '2019-05-08', '09:00:00', '18:00:00'),
(8, '2019-05-08', '10:00:00', '18:00:00'),
(9, '2019-05-08', '09:00:00', '18:00:00'),
(10, '2019-12-27', '13:00:00', '15:00:00'),
(11, '1970-01-01', '10:00:00', '10:00:00'),
(12, '1970-01-01', '10:00:00', '10:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pageapp`
--

CREATE TABLE IF NOT EXISTS `pageapp` (
  `id` int(11) NOT NULL,
  `pg_title` varchar(200) CHARACTER SET utf8 NOT NULL,
  `pg_slug` varchar(100) CHARACTER SET utf8 NOT NULL,
  `pg_descri` longtext CHARACTER SET utf8 NOT NULL,
  `pg_status` int(50) NOT NULL,
  `pg_foot` int(50) NOT NULL,
  `crated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pageapp`
--

INSERT INTO `pageapp` (`id`, `pg_title`, `pg_slug`, `pg_descri`, `pg_status`, `pg_foot`, `crated_date`) VALUES
(1, 'support', 'support', '<p><img alt="" src="https://aplikasi.shop/demo/delivery/images/main002-960w.jpg" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>GOODFOODの使用を開始するにはどうすればよいですか？</strong></h3>\r\n\r\n<p>アプリをダウンロードして、配送先住所を追加します。<br />\r\nあなたの街の最高のレストランのフルメニューから選択してください。<br />\r\n注文をして、食事の準備と配達を行ってください！<br />\r\n混雑したエリアに料金を払うのはなぜですか？<br />\r\n追加料金により、信頼性が確保されます。特定の地域には、配達可能な配達パートナーよりも多くの注文がある場合があります。この料金により、特定の地域でGOODFOODが忙しい場合でも、必要なときに好きな食べ物を手に入れることができます。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>混雑したエリアの料金は予約料金と異なりますか？</strong></h3>\r\n\r\n<p>はい。予約料金は、当社のサービスを使用するために支払う標準料金です。この追加料金は、GOODFOODが特定のエリアで忙しい場合にのみ表示されます。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>混雑したエリアに料金があるかどうかを知るにはどうすればよいですか？</strong></h3>\r\n\r\n<p>レストランをクリックする前に、レストラン名とETAの下に矢印アイコンが表示されます。レストランをクリックすると、支払っている追加料金の正確な金額も表示されます。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>混雑したエリアの料金はどのように計算されますか？</strong></h3>\r\n\r\n<p>価格は動的であり、サービスを利用したい人の数と、特定の地域で道路にいる配達パートナーの数を考慮して計算されます。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>混雑している地域では、市内のすべてのレストランに追加料金がかかりますか？</strong></h3>\r\n\r\n<p>いいえ、まったくありません。追加料金は、特定の地域でGOODFOODが混雑している場合にのみ表示され、注文数が配送パートナーの数と釣り合うとなくなります。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>この料金はいつまで続きますか？</strong></h3>\r\n\r\n<p>混雑したエリアの料金は動的であるため、特定のエリアの配達パートナーの空き状況と同じエリアでのGOODFOODの需要に応じて出入りする場合があります。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>食物アレルギーがある場合、または注文を変更する場合はどうなりますか？</strong></h3>\r\n\r\n<p>GOODFOODを通じて注文するほとんどの食事について、特別な指示を追加して、必要な変更についてレストランに知らせることができます。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>注文に問題がある場合はどうなりますか？</strong></h3>\r\n\r\n<p>私たちは助けるためにここにいます。注文が完了したら、アプリ内からサポートチームに連絡するだけで、問題を解決できます。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>配達パートナーは食事をドアやオフィスに持ち込みますか？</strong></h3>\r\n\r\n<p>あなたの配達パートナーはあなたのドアにあなたの注文を持って来ます。外に出ることが最も簡単な場合は、それでも問題ありません。いつでも縁石で配達パートナーに会うことができます。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>配達パートナーにチップを渡す必要はありますか？</strong></h3>\r\n\r\n<p>ヒントは含まれておらず、期待も必要もありません。あなたが注文するたびにあなたの経験を評価することができます-あなたがUberライドを評価するのと同じように。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>&nbsp;</h2>', 0, 0, '2019-12-21 08:30:05'),
(2, 'about us', 'about-us', '<h3><img alt="" src="https://aplikasi.shop/demo/delivery/images/main001-960w.jpg" />​</h3>\r\n\r\n<h3><strong>どこよりもオイシイピザ、パスタ、デザートをつくる。日本人のためのオイシイをつくる。<br />\r\nそれが、「GOOD FOOD」の出発点でした。</strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="https://aplikasi.shop/demo/delivery/images/page001-960w.jpg" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>「30分以内にお届けいたします」。これは、ほかの宅配ピザチェーンが揃ってかかげていたキャッチフレーズです。</p>\r\n\r\n<p>そしてそれが話題となり宅配ピザというものが一般に広く知られるようになりました。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="https://aplikasi.shop/demo/delivery/images/page005-960w.jpg" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>しかし「GOOD FOOD」は宅配時間で話題になるより、ピザのおいしさが人々の話題になりたいと考えました。<br />\r\n「ピザってこんなにおいしいものなんだ」とひとりでも多くの方に実感してもらいたいと考えたのです。<br />\r\n私たちの商品開発コンセプトは、「毎日、日本人がたべても飽きないピザをつくる」でした。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="https://aplikasi.shop/demo/delivery/images/page004-960w.jpg" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>本場のイタリアやアメリカのピザを自由な発想で日本人向けにアレンジ。</p>\r\n\r\n<p>まったく新しいジャパニーズ・ピザを開発しました。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>「GOOD FOOD」は売上げ、そして店舗数でも宅配ピザ市場では国内No.1の座を獲得しました。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="https://aplikasi.shop/demo/delivery/images/page003-960w.jpg" /></p>\r\n\r\n<p><br />\r\nしかしそれは私たちにとってはひとつの通過点にしか過ぎません。なぜなら、もっともっと多くのお客様に「GOOD FOOD」の味を知っていただきたいからです。<br />\r\nひとりでも多くの方に私たちのおいしさをお届けしたい。<br />\r\nこれは私たちの決して変わることのない願いであり、目標です。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>&nbsp;</h2>', 0, 0, '2019-12-21 08:39:27'),
(3, 'terms and condition', 'terms-and-condition', '<h2><img alt="" src="https://aplikasi.shop/demo/delivery/images/main003-960w.jpg" /></h2>\r\n\r\n<h1>利用規約</h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>第1条（総則）</strong></h3>\r\n\r\n<p><br />\r\nGOODFOOD利用規約（以下「本規約」といいます）は、利用者の皆様（以下「会員」といいます）が株式会社GOODFOOD（以下「GOODFOOD」といいます）が運営する、商品購入サービス（商品購入サービスに付随するメール配信サービスや第三者が商品購入サービスのために提供するサービスを含み、以下「本サービス」といいます）を利用するにあたり、本サービスを利用する全ての会員が遵守すべき事項及び会員と当社との関係を定めるものです。会員とGOODFOODとの本サービスの利用及び運営等にかかる一切の事項に適用されるものとし、会員は本規約を確認・承諾の上、本サービスを利用するものとします。<br />\r\nGOODFOODは、会員が本サービスを利用した時点で、会員が本規約の全ての記載内容に同意したものとみなします。</p>\r\n\r\n<p><br />\r\n本サービスを利用するためには、本規約の全ての条項に同意の上、第3条の会員登録手続により本サービスの会員となる必要があります。<br />\r\nGOODFOODは、会員に対する事前又は事後の通知なしに本規約を改定できるものとし、本規約の改定後は、改定後の本規約を適用するものとします。また、会員が本規約の改定後に本サービスを利用した場合、改定後の本規約に同意したものとみなします。</p>\r\n\r\n<p><br />\r\n変更後の本規約は、GOODFOODが別途定める場合を除いて、本サービスを提供するGOODFOODのWEBサイト（以下「本サイト」といいます）上で表示された時点より効力を生じるものとし、会員が、本規約の変更の効力が生じた後に本サービスをご利用になる場合には、変更後の本規約の全ての記載内容に同意したものとみなされます。</p>\r\n\r\n<p><br />\r\n本規約とは別に、当社から会員に対して、個別に通知した事項、本サイト上に表示する本サイト又は本サービス利用上の規定・ご利用上の注意・ご利用ガイドなどを個別規約を定める事ができます。前項における個別規約は、本規約の一部を構成するものとし、本規約と矛盾抵触する場合については、個別規定が優先されて適用されるものとします。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><br />\r\n<strong>第2条（通知）</strong></h3>\r\n\r\n<p><br />\r\n毎食は本サイト上に表示することにより、会員に対し必要な事項を通知します。前項の通知は、当該通知が本サイトに掲載された時点から、当該通知の効力が生じるものとします。<br />\r\n当社が会員に対する通知を電子メールによって行うときは、当社が会員に対して、登録された会員のメールアドレスに宛てて電子メールを送信した時点から、当該通知の効力が生じるものとします。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><br />\r\n<strong>第3条（会員登録）</strong></h3>\r\n\r\n<p><br />\r\n本サイト上で会員登録を希望する会員は、本規約の全ての条項に承認のうえ登録を申請できものとし、当社所定の手続により、本サービスの会員として登録される必要があります。本サービスの利用を希望する者が当社に対し会員登録手続を申し込んだ場合、本規約の全ての条項に同意したものとみなします。<br />\r\n会員登録手続は、当社が前項の申込に対して承諾することにより、完了するものとし、以後、会員登録手続が完了した者を｢会員｣とします。なお、当社は、当社の基準に基づき前項の申込に対して会員登録を認めないことがあります。当社は、会員登録を認めない場合においても、その理由の開示をせず、その他何らの義務又は責任を負いません。</p>\r\n\r\n<p><br />\r\nGOODFOODは会員に対し、ID･パスワードを付与します。会員は、自己の責任において会員ID･パスワードを厳重に管理することとし、GOODFOODは会員の過失の有無にかかわらず、会員ID･パスワードを他者が利用したことにより被った会員及び第三者の損害について、一切の責任を負わないものとします。<br />\r\n会員は、会員登録申請時その他GOODFOODからの依頼により申告する際、いかなる虚偽の申告も行わないものとします。また、IDやニックネーム等を会員が自身の選択により定めることができる場合、当該IDやニックネーム等に第三者に不快感を与えるような文言や第三者のプライバシー権を侵害するような文言を利用してはならないものとします。会員は、GOODFOODから、登録事項の確認、証明のための資料の提出を求められた場合にはGOODFOODが指定する期間内にこれに応じるものとします。<br />\r\n会員は、登録事項の確認、証明に必要とされる資料を当社より求められた場合は当該資料を速やかに提出し、当社はこれにより取得した会員の情報を当社が別途定める「プライバシーポリシー」に従って取り扱います。</p>\r\n\r\n<p><br />\r\n会員は、GOODFOODに申告した情報に変更が生じた場合、速やかに本サイト所定の方法によりGOODFOODに届け出るものとし、当該届け出がなかった場合について生じるトラブルについてGOODFOODを免責するものとします。</p>\r\n\r\n<p><br />\r\n本規約に違反した等の理由により、GOODFOODが不適切と判断した会員については、GOODFOODは会員の事前の承諾なしに、そのサービスの利用停止又は会員資格の抹消を行うことができるものとします。また、会員登録後一定期間に一度も当サービスにログインをしなかった場合には当サービスのデータメンテナンスの際に会員登録が抹消されることがあります。当社が会員資格の抹消を行った場合における損害についての一切の責任は負いません。</p>\r\n\r\n<p><br />\r\n会員は、当社が発行する会員ID及びパスワードを第三者に貸与、譲渡及び質入等をすることはできません。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3><strong>第4条（本サービスの内容）</strong></h3>\r\n\r\n<p><br />\r\n利用者が当サイト上、当サイトの加盟店に対し注文行為（以下、取引と言います。）を行い、弊社がその注文情報を受け付けた場合、当サイト上にて、注文を受け付けた旨を表示し、その表示をもって受け付けを完了とします。<br />\r\n弊社は、受け付け完了の表示を行うと同時に、登録メールアドレス宛に、受け付け完了メールを送信します。利用者は、受け付け完了メールは、理由の如何を問わず、送信失敗、遅延、未着が発生する場合がある旨を予め了承するものとし、弊社は、これにより発生した一切の損害について、如何なる責任も負わないものとします。</p>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3><strong>第５条（基準時間）</strong></h3>\r\n\r\n<p><br />\r\n本サービスの提供にあたって基準となる時刻は、全て当社のサーバー内で管理されている時刻によるものとし、本サービスにて表示される時間情報について、当社はその精緻さや有用性を保証しないものとします。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>第６条（GOODFOODの免責）</strong></h3>\r\n\r\n<p><br />\r\nソフトウェア及びハードウェアのトラブルについて<br />\r\nGOODFOODは、会員が使用する機器、設備又はソフトウェアが本サービスの利用に適さない場合であっても、サービスの変更、改変等を行う義務を負わないものとします。<br />\r\nGOODFOODは、アクセス過多、その他予期せぬ要因に基づく本サービスの表示速度の低下、障害、データの消失又はデータへの不正アクセス等により生じた損害又は不利益、その他本サービスに関連して会員に生じた損害又は不利益についても、一切責任を負いません。<br />\r\nGOODFOODは、会員が本サービスを利用する際に発生する通信費用や設備投資について、一切負担しないものとします。</p>\r\n\r\n<p><br />\r\nGOODFOODは、本サービスに関連して送信される電子メール及びＷＥＢコンテンツに、コンピュータウィルス等の有害なものが含まれていないことを保証しないものとします。<br />\r\n本サービスにおいてコンテンツの表示制御にCookie及びJavaScriptが使用されていることを了承し、会員が設定を変更することによりCookie及びJavaScriptの機能を停止する場合には、本サービスを最適な状態で受けることができなくなることに同意するものとします。<br />\r\nGOODFOODが相当の安全策を講じたにもかかわらず、以下の事由により損害が生じた場合、GOODFOODはいかなる責任も負わないものとします。</p>\r\n\r\n<p><br />\r\n通信回線やコンピュータ等に障害が生じたことにより、本サービスのシステムの中断・遅滞・中止等によって発生した損害<br />\r\n本サイトが改竄されたことにより生じた損害<br />\r\n本サービスに関するデータへの不正アクセスにより生じた損害<br />\r\nサービスの利用について</p>\r\n\r\n<p><br />\r\nGOODFOODは、会員が本サービスを利用できなかったことにより発生した一切の損害について、いかなる責任も負わないものとします。</p>\r\n\r\n<p><br />\r\nGOODFOODは、本サービスで会員が商品を購入し、その商品が会員の希望を満たすこと、有用であること等その他一切を保証しません。また、GOODFOODは、会員が会員の責めに帰すべき事由（購入申請時に送信された情報の誤りを含みます。）により商品の受取りが遅延すること、又は当該遅延によって発生する店舗サービスへの影響について、なんら責任を負うものではありません。<br />\r\n前項に基づき本サービスの停止等の措置を受けた会員は、当該措置の理由の開示を求めることができないものし、当該措置について争わないものとします。</p>\r\n\r\n<p><br />\r\n当社は、掲載されたレストラン及びおとりよせ商品に関する情報については、いかなる保証もいたしません。ご予約時、ご注文時、またはお出かけの前に、住所・営業時間・定休日などを電話等の方法により直接お店に確認されることをお勧めいたします。また、掲載されたレストラン、おとりよせ商品に関する情報によってお客様に生じた損害や、お客様同士のトラブル等について、当社は一切の補償および関与をいたしません。</p>\r\n\r\n<p><br />\r\nユーザーレビュー機能について<br />\r\n当社は、掲載されたユーザーレビューの内容に関していかなる保証もいたしません。お客様のご判断でご利用ください。また、掲載されたユーザーレビューによって生じたお客様の損害（お客様が作成した各種コンテンツによるコンピュータ・ウィルス感染被害なども含みます）や、お客様同士のトラブル等に対し、当社は一切の補償および関与をいたしません。<br />\r\nお客様皆様に快適にご利用いただくため、下記に該当・類似する口コミが発見された場合、予告無く、当該ユーザーレビューをGOODFOOD上から削除する場合があります。レビュー削除の理由については開示をせず、その他何らの義務又は一切の責任を負いません。</p>\r\n\r\n<p><br />\r\n公序良俗に反するもの<br />\r\nGOODFOODの趣旨、または口コミの対象となるお店や商品と関係無いもの<br />\r\n有害なプログラム・スクリプト等を含むもの<br />\r\n営利を目的としたものや個人的な売買・譲渡を持ちかける内容、宣伝行為<br />\r\nGOODFOODの信用を毀損するおそれのある行為。</p>\r\n\r\n<p><br />\r\nその他、GOODFOODの管理運営を妨げる等、当社が不適切と判断したもの<br />\r\n当社は、GOODFOODからリンクされた第三者が運営する外部サイトに関して、いかなる保証もいたしません。お客様のご判断でご利用ください。また、リンク先で生じた損害や、お客様同士のトラブル等に対し、当社は一切の補償および関与をいたしません。</p>\r\n\r\n<p><br />\r\n当社の会員に対する責任は、いかなる場合でも、会員が購入した商品代金を上限とし、また、当社は、会員の逸失利益、間接損害、特別損害又は弁護士費用に係る損害を賠償しないものとします。<br />\r\n上記各項記載事項の他、本サービスのご利用にあたり会員に発生した一切の損害について、GOODFOODは原則として責任を負いません。但し、GOODFOODの故意又は重大な過失により会員に損害を与えた場合は、この限りではなく、GOODFOODは、会員について発生した直接かつ通常の損害を商品の対価を上限として賠償するものとします。GOODFOODの責任に帰すべからざる事由から発生した損害、GOODFOODの予見の有無にかかわらず、特別の事情から生じた障害、逸失利益については責任を負いかねますので、あらかじめご了承ください。</p>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3><br />\r\n<strong>第７条（登録の取消等）</strong></h3>\r\n\r\n<p><br />\r\n会員が以下の各号のいずれかの事由に該当する場合、GOODFOODは本サービスの利用を許諾せず、事前に通知若しくは催告することなく、会員の退会処置、本サービスの全部又は一部の利用停止、会員登録を取り消し、又はその他当社が必要と判断する措置を講ずることができます。会員はこれに対して一切異議を申し立てず、またこれにより損害又は不利益を被ったとしても、GOODFOODを免責し、賠償請求その他一切の請求を行わないものとします。</p>\r\n\r\n<p>過去を含め本規約又は法令に違反したとき<br />\r\n本サービスの運営を妨害したとき<br />\r\n会員ＩＤ又はパスワード及び本サービスを不正に使用し又は使用させた場合<br />\r\n会員登録事項に虚偽の内容又は不足があったとき又はそのおそれがあるとき<br />\r\n指定アドレスに電子メールアドレスが不通のとき、又はGOODFOODからの照会、資料提出等の要請に対して速やかに対処しないとき<br />\r\nクレジットカードの与信確認に問題が生じたとき<br />\r\nいわゆる反社会的勢力もしくは反社会的活動を行う団体に所属し、又はこれらと密接な関係を有するとき<br />\r\n会員に対し、差押、仮差押、仮処分、強制執行、破産等の申し立てがなされた場合、又は、会員が自ら破産、債務整理の申し立てをした場合<br />\r\n死亡したとき、又は後見開始、保佐開始若しくは補助開始の審判を受けたとき<br />\r\nその他、当社が登録の継続を適当でないと判断したとき</p>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3><br />\r\n<strong>第８条（退会）</strong></h3>\r\n\r\n<p><br />\r\n会員は、当社所定の手続に従い退会することができます。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>第９条（本サービスの中断又は停止について）</strong></h3>\r\n\r\n<p><br />\r\n当社は、以下の各号のいずれかの事由に該当する場合、会員に対して事前の通知又はその他何らの告知をすることなく、本サービスの利用を全部又は一部を一時的に中断、又は永久的に停止する場合があります。かかる場合において、会員に何らかの損害又は不利益が生じたとしても、当社は一切責任を負わないものとします。</p>\r\n\r\n<p>システムの保守、システム障害対応、天災等の不可抗力、その他技術上、運用上の理由により、本サービスの提供を中断する必要があると判断した場合。<br />\r\n本サービスのリニューアル若しくはデザイン変更又は機能拡張等を行う場合<br />\r\nその他、当社が停止又は中断を必要と判断したとき。</p>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3><br />\r\n<strong>第１０条（個人情報の取り扱い）</strong></h3>\r\n\r\n<p><br />\r\nGOODFOODは、別途定める「プライバシーポリシー」に則り、会員の個人情報を取り扱うものとします。なお、GOODFOODは、会員からGOODFOODへいただいたご意見やお問い合せ内容をもとに、個人を特定できない形式による統計データを作成し、当該データを利用させていただくことがあります。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>第１１条（知的財産権）</strong></h3>\r\n\r\n<p><br />\r\n本サイト上を構成する文章、画像、プログラムその他のデータ等についての一切の権利（所有権、商標権、特許権、肖像権、意匠権その他一切の知的財産権）は、すべて当社又は当社にその利用を許諾した権利者に帰属し、、会員、および非会員についても、方法又は形態の如何を問わず、これらについて当社の事前の書面による承諾なく、複製、譲渡、貸与、翻訳、改変、転載、公衆送信（送信可能化を含む。</p>\r\n\r\n<p>以下同じ）、配布、出版、営業使用等その他一切の利用を行ってはならないものとします。なお、会員が本サービス上で投稿したコメントその他データの著作権は、当該会員に留保されるものとします。但し、本サービスを利用して投稿・編集された文章及び画像その他の著作物については、期間又は地域その他何らの事項についても制限なく、当社が無償で自由に利用（複製、譲渡、貸与、翻訳、改変、転載、公衆送信、配布、出版、営業使用等その他一切の利用を含む）できるものとします。</p>\r\n\r\n<p>また、会員はかかる著作物について、当社及び当社の指定する第三者に対し、著作者人格権を行使しないものとします。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>第１２条（禁止事項）</strong></h3>\r\n\r\n<p><br />\r\n会員は、本サービスを利用するにあたり、以下の各行為をしてはならないものとします。</p>\r\n\r\n<p>本規約に違反する行為<br />\r\n犯罪行為若しくは犯罪行為に結びつく行為又はそのおそれのある行為<br />\r\n当社、参加加盟店、他の会員又は第三者の著作権、商標権、特許権、意匠権その他一切の知的財産権を侵害する行為、又は侵害するおそれのある行為<br />\r\nGOODFOOD、参加加盟店又は第三者の財産権、プライバシー権、肖像権その他の権利を侵害する行為又はそのおそれのある行為<br />\r\n当社、参加加盟店、他の会員又は第三者へ対する誹謗中傷行為<br />\r\n本サービスを営利目的で利用する行為<br />\r\n本サービスの運営を妨げること、または、当社の信用を毀損すること<br />\r\nシステムへの不正アクセス等本サービスの運営を妨げる行為<br />\r\n故意に本サービスのシステムへ負荷をかける行為<br />\r\nコンピューター・ウィルスその他の有害なコンピューター・プログラムを含む情報を送信する行為<br />\r\nGOODFOOD又は第三者の信用を損なう行為<br />\r\n他人になりすまして情報等を送信する行為<br />\r\n法令、公序良俗若しくは本規約に違反する行為又はそのおそれのある行為<br />\r\n本サービスとは関係のない団体やサービス、活動にたいしての勧誘行為<br />\r\n虚偽、不正確又は誤解を招くような内容を含む情報等を掲載する行為<br />\r\n差別的表現を掲載する行為<br />\r\nわいせつ表現又は児童ポルノその他の本サービスにおいて不適当な表現を掲載する行為<br />\r\n暴力的又はグロテスクな文章又は画像、及びその他一般の会員が不快に感じる画像又は言葉その他の表現の掲載行為<br />\r\n異性との出会い等を目的とする行為<br />\r\n営利・非営利を問わず、商品又はサービスの取引を目的とする情報の掲載行為<br />\r\nその他GOODFOODが不適切と判断する行為</p>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3><br />\r\n<strong>第１３条（本規約の有効性）</strong></h3>\r\n\r\n<p><br />\r\n本規約の規定の一部が法令に基づいて無効と判断されても、本規約のその他の規定は有効とします。<br />\r\n本規約の規定の一部がある会員との関係で無効とされ、又は取り消された場合でも、本規約はその他の会員との関係では有効とします。</p>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3><br />\r\n<strong>第１４条（問い合わせについて）</strong></h3>\r\n\r\n<p><br />\r\n当社に対する質問、問い合わせその他一切の連絡及び通知（以下あわせて「質問等」といいます）は、下記の連絡先に電子メール又は電話により行うものとし、当社は必要に応じてこれに回答します（但し、回答期限は設けないものとします）。</p>\r\n\r\n<p>電子メールでの質問等：support@GOODFOOD.com</p>\r\n\r\n<p>第15条（準拠法及び管轄裁判所）<br />\r\n本規約は日本法に従って解釈され、本規約に関連して当社と会員との間に紛争が生じた場合は、訴額に応じて東京簡易裁判所又は東京地方裁判所を第一審の専属的合意管轄裁判所とします。</p>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3>以上</h3>', 0, 0, '2019-12-21 08:42:28');

-- --------------------------------------------------------

--
-- Table structure for table `pincode`
--

CREATE TABLE IF NOT EXISTS `pincode` (
  `pincode` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pincode`
--

INSERT INTO `pincode` (`pincode`) VALUES
(360004),
(360005),
(360001),
(360002),
(360003);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `product_description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `product_image` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `category_id` int(11) NOT NULL,
  `in_stock` int(11) NOT NULL,
  `price` double NOT NULL,
  `unit_value` double NOT NULL,
  `unit` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `increament` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_description`, `product_image`, `category_id`, `in_stock`, `price`, `unit_value`, `unit`, `increament`) VALUES
(43, 'Tomato', '', '122.jpg', 2, 1, 8, 1, 'Kg', 0.5),
(44, 'Onion', '', '10.jpg', 2, 1, 4, 1, 'Kg', 0.5),
(45, 'Carrot', '', '11.jpg', 2, 1, 4, 1, 'Kg', 0.5),
(46, 'Lemon', '', '9.jpg', 2, 1, 6, 1, 'Bag', 1),
(48, 'Orange', '', '8.jpg', 1, 1, 4, 0.5, 'Kg', 0.5),
(49, 'Watermelon', '', '7.jpg', 1, 1, 20, 1, 'One', 1),
(55, 'MIlk butter & cream', '', 'MIlk_butter_cream.jpg', 22, 1, 20, 10, 'KG', 1),
(56, 'Bread Buns & Pals', '', 'Bread_Buns_Pals.jpg', 23, 1, 10, 1, 'KG', 1),
(57, 'Dals Mix Pack', '', 'Flours-and-Sooji-600x315.jpg', 11, 1, 20, 1, 'KG', 1),
(58, 'buns-pavs', '', 'buns-pavs.jpg', 23, 1, 15, 1, 'packet', 1),
(59, 'cakes', '', 'cakes.jpg', 23, 1, 1200, 1, 'kg', 1),
(60, 'Channa Dal', '', 'Channa_Dal.jpg', 10, 1, 80, 1, 'kg', 1),
(61, 'Toor Dal', '', 'toor_dal.jpg', 10, 1, 80, 1, 'kg', 1),
(62, 'Wheat Atta', '', 'Wheat_Atta.jpg', 11, 1, 25, 1, 'kg', 1),
(63, 'Beson', '', '64c7b258200279da31ced10d3fb3f693.png', 11, 1, 30, 1, 'kg', 1),
(64, 'Almonds', '', 'Almonds.jpg', 18, 1, 1200, 1, 'gm', 1),
(65, 'Packaged Drinking', '', 'Packaged_Drinking.png', 26, 1, 15, 500, 'ml bottle', 1),
(66, 'Cola drinks', '', 'Cola_drinks.jpg', 27, 1, 30, 150, 'ml pack', 1),
(67, 'Other soft drinks', '', 'Other_Soft_Drinks.png', 27, 1, 50, 150, 'ml pack', 1),
(68, 'Instant Noodles', '', 'Instant_Noodles.jpg', 30, 1, 72, 70, 'gm of 6 pa', 1),
(69, 'Cup Noodles', '', 'Cup_Noodles.jpg', 30, 1, 60, 1, 'pack', 1),
(70, 'Salty Biscuits', '', 'Salty_Biscuits.jpg', 31, 1, 30, 1, 'packet', 1),
(71, 'cookie', '', 'Cookies.jpg', 31, 1, 50, 1, 'packet', 1),
(72, 'Sanitary pads', '', 'Sanitary_pads.jpg', 33, 1, 50, 1, 'pack', 1),
(73, 'sanitary Aids', '', 'Sanitary_Aids.jpg', 33, 1, 80, 1, 'pack', 1),
(74, 'Toothpaste', '', 'toothpaste.jpg', 34, 1, 45, 1, 'packet', 1),
(76, 'Hair oil', '', 'Hair_oil.jpg', 35, 1, 90, 1, 'Bottle', 1),
(77, 'Shampoo', '', 'Shampoo.jpg', 35, 1, 150, 1, 'Bottle', 1),
(79, 'Pure & pomace olive', '', 'Pure_pomace_olive1.jpg', 42, 1, 890, 1, 'Bottle', 1),
(80, 'ICE cream', '', 'ice_cream.jpg', 42, 1, 35, 1, 'gm', 1),
(81, 'Theme Egg', '', 'ic_payment_option.png', 24, 1, 50, 10, 'Pics', 0),
(82, 'Amul Milk', '', 'milk.jpg', 22, 1, 60.5, 1, 'Litter', 0),
(83, 'トマト・牛肉ピザ', 'デリバリー業務の最大の利点は、自宅にまで商品を運んでもらえることです。天候が悪い場合や外出が億劫な時に連絡一つで商品を持ってきてくれるのですから、ユーザにとってこんなに助かることはないです。', 'pizza001.jpg', 52, 1, 1250, 1, '皿', 0),
(84, 'エビパスタ', '小麦を水や卵で練って作ったものを言うのだが、生の手打ちパスタと乾燥パスタがある。 そして、パスタの品質に関してはイタリアでは法律によって定められている。', 'pasta001.jpg', 53, 1, 1260, 1, '皿', 0),
(86, 'イチゴジュース', 'いちごジュースの簡単おいしいレシピ（作り方）が261品! 「いちごジュース」「苺 イチゴジュース」「濃厚いちごジュース」「いちごバナナジュース」', 'drink0021.png', 46, 1, 300, 1, 'グラス', 0),
(87, 'マカロニ', 'マカロニきなこ. 超速！明太クルル. しゃきしゃきレタスの卵スープパスタ. 野菜たっぷりスープカレーパスタ. ブロッコリーとコーンのクリームパスタ. キャベツとソーセージのトマト ...', 'pasta002.jpg', 53, 1, 1360, 15, '皿', 0),
(88, 'チーズパスタ', '人気で簡単な、チーズ系パスタのレシピを紹介します。粉チーズやとろけるチーズなどお好みのチーズを使って美味しいスパゲティを作っちゃいましょう♪', 'pasta003.jpg', 53, 1, 1500, 1, '皿', 0),
(89, '牛肉パスタ', '牛肉を使ったパスタといえばこれ！牛肉トマト煮込みパスタ、牛肉入り和風パスタ、牛肉きのこ生パスタ、牛肉和風パスタ、牛肉ステーキなどなど、どれも美味しそうですよね。牛肉を使った定番パスタ料理をまとめました。牛肉といえば、ごぼう、薄切り、白菜といった料理にもバッチリですね。牛肉関連レシピも最後でご紹介しています！', 'pasta004.jpg', 53, 1, 1630, 0, '皿', 0),
(90, 'チーズクラッシュピザ', '「New Yorker 1 キロ ウルトラチーズ」とは、直径40センチの新シリーズ「ニューヨーカー」にこだわりのオリジナルモッツァレラ100%チーズが1キロどっさりと乗っていることで、ネットの話題をかっさらった話題のピザです。', 'pizza002.jpg', 52, 1, 2740, 1, '皿', 0),
(91, '卵ピザ', 'オリジナルピザと卵トッピング。\r\nサイズはスモールからビッグサイズがあります。\r\nぜひご注文ください。', 'pizza003.jpg', 52, 1, 2020, 1, '皿', 0),
(92, 'ペパーチーズピザ', '新着商品のペパーチーズぴざ。\r\n新しいメニューなので、今すぐオーダーするお客様に30％OFF!', 'pizza004.jpg', 52, 1, 2400, 0, '皿', 0),
(93, 'カクテル', 'おすすめのカクテルをたった30だけお伝えします。まだ沢山紹介したいのですが、初心者でも飲みやすく、自宅でも作りやすいものを集めました。', 'drink0011.png', 46, 1, 1230, 1, 'グラス', 0),
(94, 'レモンスカッシュ', '果汁・果肉が入った、スクイーズドタイプの本格的な味わいのレモンスカッシュです。シュワッとはじける爽快感をお楽しみください。 ', 'drink006.png', 46, 1, 500, 1, 'グラス', 0),
(95, 'イチゴのショートケーキ', 'お口の中でふわっととろけ『いちごのショートケーキ』. ふわっとした生クリームと苺の甘みと酸味がお口の中で広がります。お店で手作り！作りたてににこだわっています♪保存料や着色料も使用していません。', 'dessert002.jpg', 47, 1, 550, 50, '', 0),
(96, 'チョコレートトリュフ', '人気のトリュフ・オー・ショコラを贅沢なテリーヌに仕上げました。 トリュフ・オー・ショコラとはひと味違う、濃厚で芳醇な味わいをお楽しみください。', 'dessert003.jpg', 47, 1, 880, 50, '', 0),
(97, '3色アイスクリーム・セット売り', '北海道生乳をたっぷり使用し、素材そのものの美味しさを堪能出来ます。自然な甘さと後味の軽やかさが特徴のバニラ、上質な香りを持つエクアドル産のカカオから作られたチョコレートがセットに。', 'dessert001.jpg', 47, 1, 630, 50, '', 0),
(98, 'ベイクド ポテト＆ソーセージ', 'ポテトとソーセージのシンプルなおいしさ。シンプルなトッピングだから生地とチーズのおいしさも引き立ちます。', 'pizza005.jpg', 52, 1, 880, 50, '', 0),
(99, 'フィリーチーズステーキ', '本場で親しまれているフィリチーズステーキをピザにしました。薄切りビーフにたっぷりのチェダーチーズソースをかけて満足な食べ応えに仕上げました。', 'pizza006.jpg', 52, 1, 980, 50, '', 0),
(100, 'ベーコンと茄子のトマトソース', '広島県産かきのクリームスープパスタに、グラナパダーノチーズをたっぷりのせました。\r\nチーズのコクが、野菜の甘みとかきの旨みを引きたてます。', 'pasta005.jpg', 53, 1, 1230, 50, '皿', 0),
(101, 'ヴェネツィア名物　スパゲッティ・ネーロ(イカ墨)', '厳選したイカ墨をコクのあるオリジナルソースに仕上げました。\r\n※大盛りはございません。', 'pasta006.jpg', 53, 1, 1420, 50, '皿', 0),
(102, 'ナポリ名物　ボンゴレビアンコ', 'たっぷりつかった殻つきあさりが、おいしさの決め手！', 'pasta007.jpg', 53, 1, 1240, 50, '皿', 0),
(103, 'フレッシュモッツァレラのカルボナーラ', '花畑牧場のミルキーなフレッシュモッツァレラチーズに、キャベツのシャキシャキ感がよく合います。\r\n食べごたえがあるのに、ヘルシーなカルボナーラ。\r\n※大盛りはございません。', 'pasta008.jpg', 53, 1, 1240, 50, '皿', 0),
(104, 'サーモンときのこの白ゴマペペロンチーノ', '風味のよいサーモンと、食物繊維豊富なきのこと合わせ、ペペロンチーノソースでさっぱりと仕上げました。', 'pasta009.jpg', 53, 1, 1240, 50, '皿', 0),
(105, '黒ビール', '自然豊かな黒部の地から、美味しいビールをお届けします。こだわりのエールビール、黒ビールをどうぞ。', 'drink005.png', 46, 1, 520, 50, '杯', 0),
(106, 'コカ・コーラ', 'コカ・コーラ』を飲みながらおいしいご飯を食べたい！」というみなさまのご要望にお応えします。', 'drink007.png', 46, 1, 120, 50, '杯', 0),
(107, 'ビール', 'ビールの原料はそれぞれの国によって異なった使い方がありますが、日本では酒税法により、麦芽・ホップ・水のほかに副原料として、米・とうもろこし（コーン）・でんぷん', 'drink008.png', 46, 1, 280, 50, '杯', 0),
(108, 'チョコレートキャラメル スウィートハート', '塩キャラメル風味のトリュフがアクセント。チョコの深～い甘さに心もとろけちゃう！※スウィートハート…「恋人」の意。', 'dessert004.jpg', 47, 1, 240, 50, '個', 0),
(109, 'りんごソルベ', '香り豊かな青森産「ふじ」の果汁を使用。甘みと酸味がそのまま味わえる爽やかソルベ。', 'dessert0021.jpg', 47, 1, 280, 50, '個', 0),
(110, 'いちごみるく', '果肉入りのいちごアイスクリームをまろやかなミルクで包み込んだやさしい味わい。', 'dessert0031.jpg', 47, 1, 280, 50, '個', 0),
(111, '抹茶ティラミス', 'コクのあるマスカルポーネの味わいに老舗の手で選び抜かれた抹茶が深みを添える和洋の新しいコラボレーション！\r\n※「北川半兵衞商店」京都宇治抹茶使用', 'dessert0011.jpg', 47, 1, 290, 50, '個', 0),
(112, 'COBA', 'asdasd', 'indomie-goreng-rendang_big1.png', 47, 1, 12, 12, 'PCS', 0);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `purchase_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`purchase_id`, `product_id`, `price`, `qty`, `unit`, `date`) VALUES
(8, 84, 20, 20, 'kg', '2019-12-27 03:38:41');

-- --------------------------------------------------------

--
-- Table structure for table `registers`
--

CREATE TABLE IF NOT EXISTS `registers` (
  `user_id` int(11) NOT NULL,
  `user_phone` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_fullname` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_email` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_password` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_image` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pincode` int(11) NOT NULL,
  `socity_id` int(11) NOT NULL,
  `house_no` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mobile_verified` int(11) NOT NULL,
  `user_gcm_code` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_ios_token` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `varified_token` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` int(11) NOT NULL,
  `reg_code` int(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registers`
--

INSERT INTO `registers` (`user_id`, `user_phone`, `user_fullname`, `user_email`, `user_password`, `user_image`, `pincode`, `socity_id`, `house_no`, `mobile_verified`, `user_gcm_code`, `user_ios_token`, `varified_token`, `status`, `reg_code`) VALUES
(130, '99748504035', 'user', 'user@gmail.com', 'ede997b0caf2ec398110d79d9eba38bb', '911126af32c5fe4c2db26aa4562cfaad.png', 363625, 935, '36, Shree Krishna Tower', 0, 'drCUkquaU80:APA91bEYFRAwyGuDrHj0KbzcxzQ0S63Y0d6KFZgbgtWUGGnGRqFqNL0TPCxVHDgJCASF-vxJnFGkWrBm4h4Ch-o1wisho6JQddqnHsMsw8gVPh6z_NtkwNDGKasrK6lsjRNUNZ15rrex', '', '', 1, 0),
(131, '8530875127', 'rajesh', 'rajesh.b.dabhi@gmail.com', 'ede997b0caf2ec398110d79d9eba38bb', '', 0, 0, '', 0, '', '', '', 1, 0),
(132, '0812135465421', 'Erik', 'erik.appkey@gmail.com', '86e916352d024f292f6ff0ae210acdfe', '', 0, 0, '', 0, '', '', '', 1, 0),
(133, '08123456789', 'Budi', 'us1@delivery.com', '86e916352d024f292f6ff0ae210acdfe', '', 0, 0, '', 0, 'cdLKjef6p74:APA91bH7uqodE2NR99bs0Uz15s34Q85qOmsaRuiFTmsIPqlDjXbe_hFvCGegyidB3KyPGrYejuhypkS0WQy71oPhdsxbNvN5Lcqjpnx6QdmGQ0z5T17CByzgMDTReclKc5Bgzsxd31TL', 'd01MdBx5Ifw:APA91bHakxrcq9I2enrUhA65cOrWjewCOJYOR3u_m-OhnkbwNje4P0V6lAVml5lsADzAPu8g690AYQhi8KmgUIlSr5cDVSAu-ZD0ehaVPsH5RzdbPO6cMapRzfFr2pgB7R6dawaNBdk6', '5dfdc40270ca75dfdc40270ca8', 1, 0),
(134, '0841256789', 'us1', 'us@delivery.com', '86e916352d024f292f6ff0ae210acdfe', '', 0, 0, '', 0, 'ecHd8FB9PPQ:APA91bE-sexI_wOKx1nWJiTuVNrG-DOlxMJ7oWzBs9tQbxjXPwzm6q86V6AdBurudepHhpupdP_FyvVbk5tXvZZWWwFLsif64FnlSa-CQtaNlSPSv4_pCMzhtVtm95QIorCoWAVowloA', '', '', 1, 0),
(135, '0812233456', 'us1', 'us2@delivery.com', '86e916352d024f292f6ff0ae210acdfe', '', 0, 0, '', 0, 'f7Y1nBAFhp8:APA91bEEWzKvEmJ6fUXSyJOvrlwqMMr7c2kX_EN0rqpjD-cj5sfmBr5mNM6KTNGyZ0JnMNMsHqnv2t-mBiu8Cxsy21oqT241Ui0sAV47QepIiR9tjp6NAzdhXl4wnbAVy7XApRyX_HFc', '', '', 1, 0),
(136, '0361238091', 'やまだ', 'yamada@gmail.com', '5732f6b23c7d2cb118c0bfcccb48ed05', '', 0, 0, '', 0, 'eCGvC6wFR80:APA91bFBVHt4m2E_q2qwsT1U2NGxh_CdAd_jzt3uQE28nmPRJSr_7ouQpzAcxbb8FiuC0nwTCGWnxaQS_wU_eBTeSP1npONjfI0yrel0eyHuI4RWksPElWagdg2RMAo2onjXJ-P6aiCU', '', '', 1, 0),
(137, '081123456789', 'erik tohir', 'erik@delivery.com', '86e916352d024f292f6ff0ae210acdfe', '', 0, 0, '', 0, 'f7Y1nBAFhp8:APA91bEEWzKvEmJ6fUXSyJOvrlwqMMr7c2kX_EN0rqpjD-cj5sfmBr5mNM6KTNGyZ0JnMNMsHqnv2t-mBiu8Cxsy21oqT241Ui0sAV47QepIiR9tjp6NAzdhXl4wnbAVy7XApRyX_HFc', '', '', 1, 0),
(138, '098778898766', 'guyi', 'us3@delivery.com', '86e916352d024f292f6ff0ae210acdfe', '', 0, 0, '', 0, 'eueownSnx8A:APA91bHlUuRVQvniVdt_kJf8QlV-ApF5WEz3N8MHOL3t16dpwhGcpEdQFz1CuFFcRDLTW_qtOcG0r9tydqCaxDlcUkYCDxVqdcPLaMAJpf73ySWhFPTtOybDBY2N8TsFluCmfRRtBl55', '', '', 1, 0),
(139, '0876655678898', 'pitiie', 'us4@delivery.com', '86e916352d024f292f6ff0ae210acdfe', '', 0, 0, '', 0, 'f7Y1nBAFhp8:APA91bEEWzKvEmJ6fUXSyJOvrlwqMMr7c2kX_EN0rqpjD-cj5sfmBr5mNM6KTNGyZ0JnMNMsHqnv2t-mBiu8Cxsy21oqT241Ui0sAV47QepIiR9tjp6NAzdhXl4wnbAVy7XApRyX_HFc', '', '', 1, 0),
(140, '+6289644282085', 'dadi', 'dadi.appkey@gmail.com', '5cc5933faff46add8560117c317d74ae', '', 0, 0, '', 0, 'dWSZhwCQATk:APA91bGpTfYY-EcdCMxAGEftGELv6gLm2ijJkHOqWjarQJuZFYo6QTM60dtQroSd9JSzdYjIpoL3dWQ3G3TRo1DjOkwgfvesQRaAYBnoz49sSsL5Bzag1MJJkfJcxOOY_XBqx31cU4YX', '', '', 1, 0),
(141, '+628563802498', 'dayu ratna', 'dayuratna76@gmail.com', '31a3d90c421e77720c3dc9f3fd0b2134', '', 0, 0, '', 0, 'dWSZhwCQATk:APA91bGpTfYY-EcdCMxAGEftGELv6gLm2ijJkHOqWjarQJuZFYo6QTM60dtQroSd9JSzdYjIpoL3dWQ3G3TRo1DjOkwgfvesQRaAYBnoz49sSsL5Bzag1MJJkfJcxOOY_XBqx31cU4YX', '', '', 1, 0),
(142, '085123456789', 'deki', 'dadidudit@gmail.com', '447ad97d40c592fbda322ec14794132a', '', 0, 0, '', 0, '', '', '', 1, 0),
(143, '08124564784564', 'Bubu', 'us5@delivery.com', '86e916352d024f292f6ff0ae210acdfe', '', 0, 0, '', 0, '', '', '', 1, 0),
(144, '0338573401', '中村太郎', 'nosuke1@gmail.com', '609866e4ea76de9de6bce22b4846f627', '', 0, 0, '', 0, 'dYwbm1Mxims:APA91bFCnV9U1tDBa1T6ctPFPodj5sbwq_peSpPMpRyhmX7nzQiazX-YMwrA3XIILEMJqLBrIQ6JURhMlbpRjjAjiqmQLIi5uTc3yWGyhkoQcAQyicGCVKHYqFtMKgFldN-Leu0whDgR', '', '', 1, 0),
(145, '08088589158', 'テスト', 'wadain@outlook.jp', '25d55ad283aa400af464c76d713c07ad', '', 0, 0, '', 0, 'd5nud42swQQ:APA91bG7XQzO0oo0lESG-tcIBlU-M9f8oG-B6wMrIO_8Dco_cODFvvvPl9tfAXxadES6U3hmDhyRB36vbKDIl2YBtNhF40g-cCJFl7ZVg97ME-Jr__okvaIrjBuGcpUc6KgagXgkJfy6', '', '', 1, 0),
(146, '089644282085', 'deki', 'daditrial@gmail.com', '6bbc24e70d9cefb4a906fda508db7d56', '', 0, 0, '', 0, 'dYwbm1Mxims:APA91bFCnV9U1tDBa1T6ctPFPodj5sbwq_peSpPMpRyhmX7nzQiazX-YMwrA3XIILEMJqLBrIQ6JURhMlbpRjjAjiqmQLIi5uTc3yWGyhkoQcAQyicGCVKHYqFtMKgFldN-Leu0whDgR', '', '', 1, 0),
(147, '07034154139', '中村太郎', 'info@appkey.jp', '1c2deb97240c2e603f821b461ee6e778', '', 0, 0, '', 0, 'dYwbm1Mxims:APA91bFCnV9U1tDBa1T6ctPFPodj5sbwq_peSpPMpRyhmX7nzQiazX-YMwrA3XIILEMJqLBrIQ6JURhMlbpRjjAjiqmQLIi5uTc3yWGyhkoQcAQyicGCVKHYqFtMKgFldN-Leu0whDgR', '', '5e1689f3049c85e1689f3049ca', 1, 0),
(148, '0881037354020', 'ayu', 'radha.appkey@gmail.com', '27072813674dadb2f3b34234adc42d84', '', 0, 0, '', 0, 'eNQeh6reOmI:APA91bFGQBIpdlaefB4VXZ391jjMAUJWqF5jNuf9jArKNKLxOgk9jPKTYviItNfFoEKZfJYCFg5TjbjZzn36XUzy4byVkdfZCL9oh36LlA-hRA30L_s9Llo5h8L9Er3X6nm0ejMs8Scv', '', '', 1, 0),
(149, '0488140666', '友利剛', 'tt@smart-routine.com', '13056ba71fdd87e972b4696fdc86249e', '', 0, 0, '', 0, 'cNDsKiyT9Yk:APA91bGezOJx04-RQMtcTEEuvXv5BfM4WCperRMAo_L91n2-VAI4b3SuUhub0i8ULST2QCNFKdMrRr42kvxGysIwDXw5BR_-XUnHUxIi27HXy4-HtErZ5myM0gFHur9lGnvrJVR4TYC-', '', '', 1, 0),
(150, '08980782257', 'appkey002', 'appkeyhar@gmail.com', '71431b1e88117facdc7584c476e09452', '', 0, 0, '', 0, 'eJQncvIgHtc:APA91bHHYHBkMvyVmPnv1h1KSsDIrJjbc7q4SBfgtu_QUazMr4SEl1c49vqPzFw1gfysbQdKyKkQV4mERgaH2B3nR5JW2gzMrAb2SZgAl05dlZy4bcYhr4Z3x1JMpcSOHzE1oWTKwDdL', '', '5ecc913d82af85ecc913d82afa', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sale`
--

CREATE TABLE IF NOT EXISTS `sale` (
  `sale_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `on_date` date NOT NULL,
  `delivery_time_from` time NOT NULL,
  `delivery_time_to` time NOT NULL,
  `status` int(11) NOT NULL,
  `note` longtext NOT NULL,
  `is_paid` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `total_kg` double NOT NULL,
  `total_items` double NOT NULL,
  `socity_id` int(11) NOT NULL,
  `delivery_address` longtext NOT NULL,
  `location_id` int(11) NOT NULL,
  `delivery_charge` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale`
--

INSERT INTO `sale` (`sale_id`, `user_id`, `on_date`, `delivery_time_from`, `delivery_time_to`, `status`, `note`, `is_paid`, `total_amount`, `total_kg`, `total_items`, `socity_id`, `delivery_address`, `location_id`, `delivery_charge`) VALUES
(74, 124, '2017-06-30', '08:30:00', '12:30:00', 0, '', 0, 0, 0, 0, 0, 'una', 0, 0),
(75, 124, '2017-06-30', '08:30:00', '12:30:00', 0, '', 0, 0, 0, 0, 0, 'una', 0, 0),
(76, 124, '2017-06-30', '08:30:00', '12:30:00', 0, '', 0, 0, 0, 0, 0, 'una', 0, 0),
(77, 124, '2017-06-30', '08:30:00', '12:30:00', 0, '', 0, 0, 0, 0, 0, 'una', 0, 0),
(78, 124, '2017-06-30', '08:30:00', '12:30:00', 0, '', 0, 0, 0, 0, 0, 'una', 0, 0),
(79, 124, '2017-06-30', '08:30:00', '12:30:00', 0, '', 0, 0, 0, 0, 0, 'una', 0, 0),
(80, 124, '2017-06-30', '08:30:00', '12:30:00', 0, '', 0, 0, 0, 0, 0, 'una', 0, 0),
(81, 124, '2017-06-30', '08:30:00', '12:30:00', 0, '', 0, 0, 0, 0, 0, 'una', 0, 0),
(82, 124, '2017-06-30', '15:00:00', '18:00:00', 3, '', 0, 108, 13, 5, 0, 'una', 0, 0),
(83, 124, '2017-07-01', '08:30:00', '12:30:00', 3, '', 0, 108, 13, 5, 0, 'una', 0, 0),
(84, 124, '2017-06-30', '15:00:00', '18:00:00', 3, '', 0, 8, 2, 1, 0, 'una', 0, 0),
(85, 124, '2017-06-30', '15:00:00', '18:00:00', 0, '', 0, 68, 5, 2, 0, 'una', 0, 0),
(86, 124, '2017-07-01', '08:30:00', '12:30:00', 0, '', 0, 80, 4, 1, 0, 'una', 0, 0),
(87, 124, '2017-07-01', '08:30:00', '12:30:00', 0, '', 0, 20, 5, 2, 0, 'una', 0, 0),
(88, 124, '2017-07-01', '08:30:00', '12:30:00', 0, '', 0, 8, 2, 1, 0, 'una', 0, 0),
(89, 124, '2017-06-30', '19:37:00', '12:38:00', 0, '', 0, 8, 2, 1, 0, 'una', 0, 0),
(90, 125, '2017-07-01', '14:35:00', '13:36:00', 0, '', 0, 8, 2, 1, 0, 'hello', 0, 0),
(91, 125, '2017-07-01', '14:35:00', '13:36:00', 0, '', 0, 8, 2, 1, 0, 'hello', 0, 0),
(92, 124, '2017-07-04', '18:30:00', '19:00:00', 3, '', 0, 64, 12, 3, 0, 'una', 0, 0),
(94, 125, '2017-07-06', '08:30:00', '09:00:00', 3, '', 0, 190, 5, 2, 0, 'shreehari web design company', 0, 0),
(95, 125, '2017-07-06', '08:30:00', '09:00:00', 3, '', 0, 190, 5, 2, 0, 'shreehari web design company', 0, 0),
(96, 125, '2017-07-05', '10:00:00', '10:30:00', 0, '', 0, 60, 3, 1, 0, 'shreehari web design company Street', 0, 0),
(97, 125, '2017-07-06', '13:30:00', '14:00:00', 2, '', 0, 3680, 7, 2, 0, 'new date', 0, 0),
(98, 124, '2017-07-06', '19:30:00', '20:00:00', 0, '', 0, 80, 4, 1, 935, '1 una\n, 1 una', 16, 0),
(99, 125, '2017-07-06', '20:00:00', '20:30:00', 0, '', 0, 340, 6, 3, 934, '14 system park.\n, 14 system park.', 18, 0),
(100, 125, '2017-07-06', '21:00:00', '21:30:00', 0, '', 0, 2770, 7, 3, 935, '25 syam park\n, 25 syam park', 15, 0),
(101, 125, '2017-07-06', '21:00:00', '21:30:00', 0, '', 0, 200, 5, 3, 935, '25 syam park\n, 25 syam park', 15, 0),
(102, 125, '2017-07-08', '10:00:00', '10:30:00', 0, '', 0, 60, 3, 1, 934, '14 system park.\n, 14 system park.', 18, 0),
(103, 124, '2017-07-07', '12:00:00', '12:30:00', 0, '', 0, 180, 4, 2, 935, '1 una\n, 1 una', 16, 0),
(104, 124, '2017-07-07', '12:00:00', '12:30:00', 0, '', 0, 180, 4, 2, 935, '1 una\n, 1 una', 16, 0),
(105, 126, '2017-07-07', '19:00:00', '19:30:00', 0, '', 0, 1315, 4, 4, 935, 'shreehari web Street\n, shreehari web Street', 19, 0),
(106, 124, '2017-07-07', '18:00:00', '18:30:00', 0, '', 0, 130, 4, 3, 935, '1 una\n, 1 una', 16, 0),
(107, 126, '2017-07-07', '18:30:00', '19:00:00', 0, '', 0, 1230, 2, 2, 935, 'shreehari web Street\n, shreehari web Street', 19, 0),
(108, 126, '2017-07-07', '18:30:00', '19:00:00', 0, '', 0, 1230, 2, 2, 935, 'shreehari web Street\n, shreehari web Street', 19, 0),
(109, 126, '2017-07-07', '18:30:00', '19:00:00', 0, '', 0, 1230, 2, 2, 935, 'shreehari web Street\n, shreehari web Street', 19, 0),
(110, 126, '2017-07-07', '18:30:00', '19:00:00', 0, '', 0, 52, 13, 2, 935, 'shreehari web Street\n, shreehari web Street', 19, 0),
(111, 126, '2017-07-07', '20:00:00', '20:30:00', 0, '', 0, 225, 5, 5, 935, 'shreehari web Street\n, shreehari web Street', 19, 0),
(112, 126, '2017-07-07', '20:00:00', '20:30:00', 0, '', 0, 225, 5, 5, 935, 'shreehari web Street\n, shreehari web Street', 19, 0),
(113, 126, '2017-07-07', '20:00:00', '20:30:00', 0, '', 0, 225, 5, 5, 935, 'shreehari web Street\n, shreehari web Street', 19, 0),
(114, 126, '2017-07-07', '20:00:00', '20:30:00', 0, '', 0, 225, 5, 5, 935, 'shreehari web Street\n, shreehari web Street', 19, 0),
(115, 126, '2017-07-07', '20:00:00', '20:30:00', 0, '', 0, 225, 5, 5, 935, 'shreehari web Street\n, shreehari web Street', 19, 0),
(116, 127, '2017-07-09', '13:00:00', '13:30:00', 0, '', 0, 2430, 4, 2, 934, '34 shaym tower\n, 34 shaym tower', 20, 0),
(117, 127, '2017-07-09', '13:30:00', '14:00:00', 0, '', 0, 180, 4, 2, 934, '34 shaym tower\n, 34 shaym tower', 20, 0),
(118, 127, '2017-07-09', '15:30:00', '16:00:00', 0, '', 0, 170, 3, 3, 934, '34 shaym tower\n, 34 shaym tower', 20, 0),
(119, 127, '2017-07-09', '15:30:00', '16:00:00', 2, '', 0, 170, 3, 3, 934, '34 shaym tower\n, 34 shaym tower', 20, 0),
(120, 128, '2017-07-09', '13:30:00', '14:00:00', 0, '', 0, 250, 4, 3, 934, '23,shreehari Street\n, 23,shreehari Street', 21, 0),
(121, 128, '2017-07-11', '09:30:00', '10:00:00', 0, '', 0, 330, 6, 3, 934, '23,shreehari Street\n, 23,shreehari Street', 21, 0),
(122, 128, '2017-07-09', '15:00:00', '15:30:00', 0, '', 0, 192, 3, 2, 934, '23,shreehari Street\n, 23,shreehari Street', 21, 0),
(123, 127, '2017-07-09', '16:30:00', '17:00:00', 1, '', 0, 1885, 5, 2, 934, '34 shaym tower\n, 34 shaym tower', 20, 0),
(124, 129, '2017-07-10', '19:00:00', '19:30:00', 0, '', 0, 110, 4, 3, 935, '23 floor plan and\n, 23 floor plan and', 22, 0),
(125, 130, '2017-07-10', '21:00:00', '21:30:00', 1, '', 0, 225, 5, 5, 935, '23, shreehari tower\n, 23, shreehari tower', 23, 0),
(126, 130, '2017-07-10', '21:00:00', '21:30:00', 0, '', 0, 225, 5, 5, 935, '23, shreehari tower\n, 23, shreehari tower', 23, 0),
(127, 130, '2017-07-13', '10:30:00', '11:00:00', 0, '', 0, 1365, 5, 5, 935, '23, shreehari tower\n, 23, shreehari tower', 23, 0),
(128, 130, '2017-07-13', '10:00:00', '10:30:00', 2, '', 0, 385, 8, 4, 935, '23, shreehari tower\n, 23, shreehari tower', 23, 0),
(129, 130, '2017-08-09', '12:00:00', '12:30:00', 0, '', 0, 1260, 2, 2, 938, 'jdhd\n, jdhd', 24, 40),
(130, 133, '2019-11-07', '09:53:00', '10:53:00', 3, '', 0, 165, 8, 2, 934, 'udhdhd\n, udhdhd', 25, 55),
(131, 133, '2019-11-07', '12:08:00', '13:08:00', 3, '', 0, 175, 8, 1, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(132, 133, '2019-11-07', '16:11:00', '17:11:00', 3, '', 0, 0, 0, 0, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(133, 133, '2019-11-07', '16:11:00', '17:11:00', 3, '', 0, 0, 0, 0, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(134, 133, '2019-11-07', '16:11:00', '17:11:00', 3, '', 0, 0, 0, 0, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(135, 133, '2019-11-07', '16:11:00', '17:11:00', 3, '', 0, 0, 0, 0, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(136, 133, '2019-11-07', '16:11:00', '17:11:00', 3, '', 0, 0, 0, 0, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(137, 133, '2019-11-07', '16:11:00', '17:11:00', 3, '', 0, 0, 0, 0, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(138, 133, '2019-11-07', '16:11:00', '17:11:00', 3, '', 0, 55, 0, 0, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(139, 133, '2019-11-07', '16:11:00', '17:11:00', 3, '', 0, 0, 0, 0, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(140, 133, '2019-11-07', '16:14:00', '17:14:00', 3, '', 0, 55, 0, 0, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(141, 133, '2019-11-07', '17:11:00', '18:11:00', 3, '', 0, 0, 0, 0, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(142, 133, '2019-11-07', '17:11:00', '18:11:00', 2, '', 0, 460, 27, 1, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(143, 133, '2019-11-07', '17:11:00', '18:11:00', 3, '', 0, 565, 37, 1, 936, 'JL. Mantap bgt\n, JL. Mantap bgt', 32, 10),
(144, 133, '2019-11-14', '14:54:00', '15:54:00', 3, '', 0, 131, 6, 3, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(145, 133, '2019-11-15', '11:11:00', '12:11:00', 3, '', 0, 430, 25, 1, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(146, 133, '2019-11-15', '11:11:00', '12:11:00', 3, '', 0, 430, 25, 1, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(147, 133, '2019-11-26', '10:11:00', '11:11:00', 3, '', 0, 115, 4, 1, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(148, 133, '2019-11-26', '10:11:00', '11:11:00', 2, '', 0, 262, 15, 2, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(149, 133, '2019-11-26', '10:11:00', '11:11:00', 1, '', 0, 190, 9, 1, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(150, 136, '2019-11-30', '11:55:00', '12:55:00', 3, '', 0, 3805, 3, 3, 934, 'test\n, test', 33, 55),
(151, 136, '2019-11-30', '13:52:00', '14:52:00', 1, '', 0, 505, 2, 2, 934, 'test\n, test', 33, 55),
(159, 133, '2019-12-09', '05:30:00', '05:30:00', 3, '', 0, 9805, 6, 2, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(160, 133, '2019-12-09', '16:12:00', '17:12:00', 3, '', 0, 2555, 2, 1, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(161, 133, '2019-12-10', '14:12:00', '15:12:00', 3, '', 0, 505, 3, 1, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(162, 137, '2019-12-10', '15:12:00', '16:12:00', 3, '', 0, 17040, 10, 2, 938, 'Hdudhdudhdudhooopp\n, Hdudhdudhdudhooopp', 34, 40),
(163, 137, '2019-12-10', '15:12:00', '16:12:00', 3, '', 0, 11040, 7, 2, 938, 'Hdudhdudhdudhooopp\n, Hdudhdudhdudhooopp', 34, 40),
(164, 139, '2019-12-10', '15:12:00', '16:12:00', 3, '', 0, 21010, 7, 1, 936, 'Huji yuuk st\n, Huji yuuk st', 35, 10),
(165, 139, '2019-12-10', '16:12:00', '17:12:00', 3, '', 0, 1060, 7, 1, 936, 'Huji yuuk st\n, Huji yuuk st', 35, 10),
(166, 133, '2019-12-10', '17:12:00', '18:12:00', 3, '', 0, 5055, 4, 1, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 55),
(167, 141, '2019-12-18', '11:48:00', '12:48:00', 0, '', 0, 16751, 9, 6, 940, 'jalan gunung mandalawangi no 2A Denpasar Barat\n, jalan gunung mandalawangi no 2A Denpasar Barat', 36, 1),
(168, 140, '2019-12-19', '10:16:00', '11:16:00', 0, '', 0, 451, 2, 2, 940, 'Jalam batusari\n, Jalam batusari', 38, 1),
(169, 133, '2019-12-19', '10:12:00', '11:12:00', 0, '', 0, 355, 2, 1, 935, 'JL. Coba Coba Coba\n, JL. Coba Coba Coba', 25, 55),
(170, 133, '2019-12-19', '10:12:00', '11:12:00', 0, '', 0, 1255, 8, 1, 935, 'JL. Coba Coba Coba\n, JL. Coba Coba Coba', 25, 55),
(171, 140, '2019-12-19', '11:12:00', '12:12:00', 0, '', 0, 3211, 5, 3, 940, 'Jalam batusari\n, Jalam batusari', 38, 1),
(172, 140, '2019-12-19', '12:15:00', '05:30:00', 0, '', 0, 451, 3, 1, 940, 'Jalam batusari\n, Jalam batusari', 38, 1),
(173, 133, '2019-12-21', '05:30:00', '05:30:00', 0, '', 0, 17955, 8, 4, 935, 'JL. Coba Coba Coba\n, JL. Coba Coba Coba', 25, 55),
(174, 133, '2019-12-21', '05:30:00', '05:30:00', 3, '', 0, 3265, 6, 3, 935, 'JL. Coba Coba Coba\n, JL. Coba Coba Coba', 25, 55),
(175, 133, '2019-12-21', '13:47:00', '14:47:00', 3, '', 0, 655, 4, 1, 935, 'JL. Coba Coba Coba\n, JL. Coba Coba Coba', 25, 55),
(176, 135, '2019-12-21', '16:53:00', '17:53:00', 0, '', 0, 870, 5, 1, 941, 'Mantapppu\n, Mantapppu', 39, 120),
(177, 144, '2019-12-21', '17:04:00', '18:04:00', 0, '', 0, 770, 2, 2, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(178, 144, '2019-12-21', '17:07:00', '18:07:00', 0, '', 0, 7720, 3, 3, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(179, 144, '2019-12-21', '17:29:00', '18:29:00', 0, '', 0, 950, 1, 1, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(180, 144, '2019-12-21', '17:48:00', '18:48:00', 0, '', 0, 5750, 3, 2, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(181, 144, '2019-12-21', '18:23:00', '19:23:00', 0, '', 0, 3580, 2, 1, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(182, 144, '2019-12-21', '18:24:00', '19:24:00', 0, '', 0, 5940, 4, 3, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(183, 144, '2019-12-21', '18:37:00', '19:37:00', 1, '', 0, 2780, 2, 1, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(184, 144, '2019-12-22', '13:06:00', '14:06:00', 2, '', 0, 3060, 1, 1, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(185, 144, '2019-12-22', '13:46:00', '14:46:00', 0, '', 0, 2930, 3, 2, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(186, 144, '2019-12-23', '16:48:00', '17:48:00', 0, '', 0, 1220, 3, 1, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(187, 138, '2019-12-23', '16:53:00', '17:53:00', 0, '', 0, 1420, 2, 1, 940, '??????????????????????????????????????\n, ??????????????????????????????????????', 42, 320),
(188, 144, '2019-12-23', '17:02:00', '18:02:00', 0, '', 0, 950, 1, 1, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(189, 144, '2019-12-23', '17:03:00', '18:03:00', 0, '', 0, 3560, 4, 2, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(190, 144, '2019-12-24', '10:50:00', '11:50:00', 0, '', 0, 4160, 6, 4, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(191, 144, '2019-12-24', '10:51:00', '11:51:00', 3, '', 0, 2720, 1, 1, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(192, 145, '2019-12-24', '16:18:00', '17:18:00', 0, '', 0, 1380, 1, 1, 941, '???3-1-11\n, ???3-1-11', 43, 120),
(193, 145, '2019-12-24', '19:03:00', '20:03:00', 0, '', 0, 3480, 4, 4, 941, '???3-1-11\n, ???3-1-11', 43, 120),
(194, 145, '2019-12-25', '17:03:00', '18:03:00', 0, '', 0, 3070, 3, 2, 939, '??1-1-1\n, ??1-1-1', 44, 740),
(195, 144, '2019-12-26', '14:39:00', '15:39:00', 0, '', 0, 820, 1, 1, 940, '??2-33-3 ?????????303??\n, ??2-33-3 ?????????303??', 40, 320),
(196, 133, '2019-12-27', '17:20:00', '18:20:00', 3, '', 0, 4960, 12, 3, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 740),
(197, 133, '2019-12-27', '17:23:00', '18:23:00', 3, '', 0, 7120, 5, 2, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 740),
(198, 133, '2019-12-27', '17:26:00', '18:26:00', 3, '', 0, 25030, 14, 6, 940, 'JL. Batusari 3 No 3 Dangin puri kelod, denpasar timur, Denpasar, Bali, Indonesia, Indonesia Raya, Asia Tenggara, Bumi\n, JL. Batusari 3 No 3 Dangin puri kelod, denpasar timur, Denpasar, Bali, Indonesia, Indonesia Raya, Asia Tenggara, Bumi', 41, 320),
(199, 133, '2019-12-28', '10:37:00', '11:37:00', 3, '', 0, 3960, 7, 1, 940, 'JL. Batusari 3 No 3 Dangin puri kelod, denpasar timur, Denpasar, Bali, Indonesia, Indonesia Raya, Asia Tenggara, Bumi\n, JL. Batusari 3 No 3 Dangin puri kelod, denpasar timur, Denpasar, Bali, Indonesia, Indonesia Raya, Asia Tenggara, Bumi', 41, 320),
(200, 133, '2019-12-28', '14:02:00', '15:02:00', 2, '', 0, 2770, 3, 3, 939, 'JL. Batusari no 3\n, JL. Batusari no 3', 31, 740),
(201, 145, '2020-01-03', '20:29:00', '21:29:00', 0, '', 0, 1370, 1, 1, 941, '???3-1-11\n, ???3-1-11', 43, 120),
(202, 145, '2020-01-06', '14:11:00', '15:11:00', 0, '', 0, 5910, 6, 4, 941, '???3-1-11\n, ???3-1-11', 43, 120),
(203, 146, '2020-01-08', '14:31:00', '15:31:00', 0, '', 0, 1590, 2, 2, 939, 'Jalan batusari renon\n, Jalan batusari renon', 45, 740),
(204, 147, '2020-01-09', '10:01:00', '11:01:00', 0, '', 0, 2470, 3, 3, 939, '??2-33-3?????????303??\n, ??2-33-3?????????303??', 46, 740),
(205, 145, '2020-01-10', '13:50:00', '14:50:00', 0, '', 0, 6970, 5, 5, 939, '??1-1-1\n, ??1-1-1', 44, 740),
(206, 147, '2020-01-16', '12:18:00', '13:18:00', 0, '', 0, 1340, 2, 1, 939, '??2-33-3?????????303??\n, ??2-33-3?????????303??', 46, 740),
(207, 147, '2020-01-16', '12:55:00', '13:55:00', 1, '', 0, 2930, 3, 2, 939, '??2-33-3?????????303??\n, ??2-33-3?????????303??', 46, 740),
(208, 145, '2020-03-10', '14:31:00', '15:31:00', 0, '', 0, 3210, 4, 3, 941, '???3-1-11\n, ???3-1-11', 43, 120),
(209, 145, '2020-04-16', '15:34:00', '16:34:00', 0, '', 0, 540, 2, 2, 941, '???3-1-11\n, ???3-1-11', 43, 120),
(210, 145, '2020-04-16', '15:34:00', '16:34:00', 0, '', 0, 670, 1, 1, 941, '???3-1-11\n, ???3-1-11', 43, 120),
(211, 145, '2020-04-16', '15:35:00', '16:35:00', 0, '', 0, 2630, 2, 2, 941, '???3-1-11\n, ???3-1-11', 43, 120),
(212, 134, '2020-04-29', '10:52:00', '11:52:00', 3, '', 0, 1290, 1, 1, 939, 'aaa\n, aaa', 47, 740),
(213, 134, '2020-04-29', '10:57:00', '11:57:00', 0, '', 0, 3290, 4, 4, 939, 'aaa\n, aaa', 47, 740),
(214, 149, '2020-05-11', '20:40:00', '21:40:00', 0, '', 0, 1290, 1, 1, 939, '??1-1-17-903\n, ??1-1-17-903', 48, 740),
(215, 149, '2020-05-14', '08:39:00', '09:39:00', 3, '', 0, 1260, 1, 1, 939, '??1-1-17-903\n, ??1-1-17-903', 48, 740),
(216, 145, '2020-05-16', '18:38:00', '19:38:00', 0, '', 0, 420, 1, 1, 941, '???3-1-11\n, ???3-1-11', 43, 120),
(217, 149, '2020-05-20', '11:59:00', '12:59:00', 0, '', 0, 2020, 3, 2, 939, '??1-1-17-903\n, ??1-1-17-903', 48, 740),
(218, 140, '2020-05-20', '16:39:00', '17:39:00', 3, '', 0, 1220, 6, 2, 940, 'Jalam batusari\n, Jalam batusari', 38, 320),
(219, 150, '2020-05-26', '13:32:00', '14:32:00', 1, '', 0, 1990, 1, 1, 939, 'Aaa\n, Aaa', 50, 740),
(220, 145, '2020-05-27', '18:54:00', '19:54:00', 0, '', 0, 1650, 2, 2, 941, '???3-1-11\n, ???3-1-11', 43, 120),
(221, 149, '2020-05-28', '15:40:00', '16:40:00', 0, '', 0, 1040, 1, 1, 939, '??1-1-17-903\n, ??1-1-17-903', 48, 740),
(222, 149, '2020-06-03', '20:05:00', '21:05:00', 0, '', 0, 2400, 4, 3, 939, '??1-1-17-903\n, ??1-1-17-903', 48, 740);

-- --------------------------------------------------------

--
-- Table structure for table `sale_items`
--

CREATE TABLE IF NOT EXISTS `sale_items` (
  `sale_item_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `qty` double NOT NULL,
  `unit` enum('gram','kg','nos') NOT NULL,
  `unit_value` double NOT NULL,
  `price` double NOT NULL,
  `qty_in_kg` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=408 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale_items`
--

INSERT INTO `sale_items` (`sale_item_id`, `sale_id`, `product_id`, `product_name`, `qty`, `unit`, `unit_value`, `price`, `qty_in_kg`) VALUES
(116, 65, 43, '', 2.5, 'kg', 1, 8, 2.5),
(117, 82, 43, '', 1, 'kg', 1, 8, 1),
(118, 82, 44, '', 3, 'kg', 1, 4, 3),
(119, 82, 46, '', 2, '', 1, 6, 2),
(120, 82, 48, '', 4, 'kg', 0.5, 4, 4),
(121, 82, 49, '', 3, '', 1, 20, 3),
(122, 83, 43, '', 1, 'kg', 1, 8, 1),
(123, 83, 44, '', 3, 'kg', 1, 4, 3),
(124, 83, 46, '', 2, '', 1, 6, 2),
(125, 83, 48, '', 4, 'kg', 0.5, 4, 4),
(126, 83, 49, '', 3, '', 1, 20, 3),
(127, 84, 48, '', 2, 'kg', 0.5, 4, 2),
(128, 85, 48, '', 2, 'kg', 0.5, 4, 2),
(129, 85, 49, '', 3, '', 1, 20, 3),
(130, 86, 49, '', 4, '', 1, 20, 4),
(131, 87, 44, '', 3, 'kg', 1, 4, 3),
(132, 87, 45, '', 2, 'kg', 1, 4, 2),
(133, 88, 45, '', 2, 'kg', 1, 4, 2),
(134, 89, 48, '', 2, 'kg', 0.5, 4, 2),
(135, 90, 48, '', 2, 'kg', 0.5, 4, 2),
(136, 91, 48, '', 2, 'kg', 0.5, 4, 2),
(137, 92, 43, '', 4, 'kg', 1, 8, 4),
(138, 92, 45, '', 2, 'kg', 1, 4, 2),
(139, 92, 48, '', 6, 'kg', 0.5, 4, 6),
(140, 93, 66, '', 3, '', 150, 30, 3),
(141, 94, 66, '', 3, '', 150, 30, 3),
(142, 93, 67, '', 2, '', 150, 50, 2),
(143, 94, 67, '', 2, '', 150, 50, 2),
(144, 95, 66, '', 3, '', 150, 30, 3),
(145, 95, 67, '', 2, '', 150, 50, 2),
(146, 96, 55, '', 3, 'kg', 10, 20, 3),
(147, 97, 57, '', 4, 'kg', 1, 20, 4),
(148, 97, 64, '', 3, '', 1, 1200, 3),
(149, 98, 57, '', 4, 'kg', 1, 20, 4),
(150, 99, 57, '', 2, 'kg', 1, 20, 2),
(151, 99, 60, '', 2, 'kg', 1, 70, 2),
(152, 99, 61, '', 2, 'kg', 1, 80, 2),
(153, 100, 65, '', 2, '', 500, 15, 2),
(154, 100, 79, '', 3, '', 1, 890, 3),
(155, 100, 80, '', 2, '', 1, 35, 2),
(156, 101, 57, '', 2, 'kg', 1, 20, 2),
(157, 101, 60, '', 1, 'kg', 1, 70, 1),
(158, 101, 74, '', 2, '', 1, 45, 2),
(159, 102, 57, '', 3, 'kg', 1, 20, 3),
(160, 103, 57, '', 2, 'kg', 1, 20, 2),
(161, 103, 60, '', 2, 'kg', 1, 70, 2),
(162, 104, 57, '', 2, 'kg', 1, 20, 2),
(163, 104, 60, '', 2, 'kg', 1, 70, 2),
(164, 105, 57, '', 1, 'kg', 1, 20, 1),
(165, 105, 60, '', 1, 'kg', 1, 70, 1),
(166, 105, 62, '', 1, 'kg', 1, 25, 1),
(167, 105, 64, '', 1, '', 1, 1200, 1),
(168, 106, 55, '', 1, 'kg', 10, 20, 1),
(169, 106, 61, '', 1, 'kg', 1, 80, 1),
(170, 106, 65, '', 2, '', 500, 15, 2),
(171, 107, 63, '', 1, 'kg', 1, 30, 1),
(172, 107, 64, '', 1, '', 1, 1200, 1),
(173, 108, 63, '', 1, 'kg', 1, 30, 1),
(174, 108, 64, '', 1, '', 1, 1200, 1),
(175, 109, 63, '', 1, 'kg', 1, 30, 1),
(176, 109, 64, '', 1, '', 1, 1200, 1),
(177, 110, 44, '', 9, 'kg', 1, 4, 9),
(178, 110, 45, '', 4, 'kg', 1, 4, 4),
(179, 111, 57, '', 1, 'kg', 1, 20, 1),
(180, 111, 60, '', 1, 'kg', 1, 70, 1),
(181, 111, 61, '', 1, 'kg', 1, 80, 1),
(182, 111, 62, '', 1, 'kg', 1, 25, 1),
(183, 112, 57, '', 1, 'kg', 1, 20, 1),
(184, 111, 63, '', 1, 'kg', 1, 30, 1),
(185, 112, 60, '', 1, 'kg', 1, 70, 1),
(186, 112, 61, '', 1, 'kg', 1, 80, 1),
(187, 112, 62, '', 1, 'kg', 1, 25, 1),
(188, 112, 63, '', 1, 'kg', 1, 30, 1),
(189, 113, 57, '', 1, 'kg', 1, 20, 1),
(190, 113, 60, '', 1, 'kg', 1, 70, 1),
(191, 113, 61, '', 1, 'kg', 1, 80, 1),
(192, 113, 62, '', 1, 'kg', 1, 25, 1),
(193, 113, 63, '', 1, 'kg', 1, 30, 1),
(194, 114, 57, '', 1, 'kg', 1, 20, 1),
(195, 114, 60, '', 1, 'kg', 1, 70, 1),
(196, 114, 61, '', 1, 'kg', 1, 80, 1),
(197, 114, 62, '', 1, 'kg', 1, 25, 1),
(198, 114, 63, '', 1, 'kg', 1, 30, 1),
(199, 115, 57, '', 1, 'kg', 1, 20, 1),
(200, 115, 60, '', 1, 'kg', 1, 70, 1),
(201, 115, 61, '', 1, 'kg', 1, 80, 1),
(202, 115, 62, '', 1, 'kg', 1, 25, 1),
(203, 115, 63, '', 1, 'kg', 1, 30, 1),
(204, 116, 58, '', 2, '', 1, 15, 2),
(205, 116, 59, '', 2, 'kg', 1, 1200, 2),
(206, 117, 57, '', 2, 'kg', 1, 20, 2),
(207, 117, 60, '', 2, 'kg', 1, 70, 2),
(208, 118, 57, '', 1, 'kg', 1, 20, 1),
(209, 118, 60, '', 1, 'kg', 1, 70, 1),
(210, 118, 61, '', 1, 'kg', 1, 80, 1),
(211, 119, 57, '', 1, 'kg', 1, 20, 1),
(212, 119, 60, '', 1, 'kg', 1, 70, 1),
(213, 119, 61, '', 1, 'kg', 1, 80, 1),
(214, 120, 57, '', 1, 'kg', 1, 20, 1),
(215, 120, 60, '', 1, 'kg', 1, 70, 1),
(216, 120, 61, '', 2, 'kg', 1, 80, 2),
(217, 121, 57, '', 2, 'kg', 1, 20, 2),
(218, 121, 60, '', 3, 'kg', 1, 70, 3),
(219, 121, 61, '', 1, 'kg', 1, 80, 1),
(220, 122, 68, '', 1, '', 70, 72, 1),
(221, 122, 69, '', 2, '', 1, 60, 2),
(222, 123, 79, '', 2, '', 1, 890, 2),
(223, 123, 80, '', 3, '', 1, 35, 3),
(224, 124, 65, '', 2, '', 500, 15, 2),
(225, 124, 66, '', 1, '', 150, 30, 1),
(226, 124, 67, '', 1, '', 150, 50, 1),
(227, 125, 57, '', 1, 'kg', 1, 20, 1),
(228, 125, 60, '', 1, 'kg', 1, 70, 1),
(229, 125, 61, '', 1, 'kg', 1, 80, 1),
(230, 125, 62, '', 1, 'kg', 1, 25, 1),
(231, 125, 63, '', 1, 'kg', 1, 30, 1),
(232, 126, 57, '', 1, 'kg', 1, 20, 1),
(233, 126, 60, '', 1, 'kg', 1, 70, 1),
(234, 126, 61, '', 1, 'kg', 1, 80, 1),
(235, 126, 62, '', 1, 'kg', 1, 25, 1),
(236, 126, 63, '', 1, 'kg', 1, 30, 1),
(237, 127, 55, '', 1, 'kg', 10, 20, 1),
(238, 127, 58, '', 1, '', 1, 15, 1),
(239, 127, 59, '', 1, 'kg', 1, 1200, 1),
(240, 127, 72, '', 1, '', 1, 50, 1),
(241, 127, 73, '', 1, '', 1, 80, 1),
(242, 128, 57, '', 3, 'kg', 1, 20, 3),
(243, 128, 60, '', 2, 'kg', 1, 70, 2),
(244, 128, 61, '', 2, 'kg', 1, 80, 2),
(245, 128, 62, '', 1, 'kg', 1, 25, 1),
(246, 129, 55, '', 1, 'kg', 10, 20, 1),
(247, 129, 59, '', 1, 'kg', 1, 1200, 1),
(248, 130, 83, '', 6, '', 19, 15, 6),
(249, 130, 84, '', 2, '', 12, 10, 2),
(250, 131, 83, '', 8, '', 19, 15, 8),
(251, 142, 83, '', 27, '', 19, 15, 27),
(252, 143, 83, '', 37, '', 19, 15, 37),
(253, 144, 83, '', 2, '', 19, 15, 2),
(254, 144, 84, '', 1, '', 12, 10, 1),
(255, 144, 85, '', 3, '', 12, 12, 3),
(256, 145, 83, '', 25, '', 19, 15, 25),
(257, 146, 83, '', 25, '', 19, 15, 25),
(258, 147, 83, '', 4, '', 19, 15, 4),
(259, 148, 85, '', 6, '', 12, 12, 6),
(260, 148, 83, '', 9, '', 19, 15, 9),
(261, 149, 83, '', 9, '', 19, 15, 9),
(262, 150, 83, '', 1, '', 1, 1250, 1),
(263, 150, 91, '', 1, '', 1, 2000, 1),
(264, 150, 94, '', 1, '', 1, 500, 1),
(265, 151, 85, '', 1, '', 1, 150, 1),
(266, 151, 86, '', 1, '', 1, 300, 1),
(267, 152, 85, '', 8, '', 1, 150, 8),
(268, 153, 85, '', 1, '', 1, 150, 1),
(269, 153, 91, '', 1, '', 1, 2000, 1),
(270, 154, 90, '', 5, '', 1, 3000, 5),
(271, 154, 93, '', 4, '', 1, 1230, 4),
(272, 155, 85, '', 2, '', 1, 150, 2),
(273, 155, 86, '', 3, '', 1, 300, 3),
(274, 156, 85, '', 3, '', 1, 150, 3),
(275, 156, 93, '', 5, '', 1, 1230, 5),
(276, 157, 85, '', 3, '', 1, 150, 3),
(277, 157, 93, '', 5, '', 1, 1230, 5),
(278, 158, 85, '', 5, '', 1, 150, 5),
(279, 158, 93, '', 7, '', 1, 1230, 7),
(280, 159, 83, '', 3, '', 1, 1250, 3),
(281, 159, 91, '', 3, '', 1, 2000, 3),
(282, 160, 83, '', 2, '', 1, 1250, 2),
(283, 161, 85, '', 3, '', 1, 150, 3),
(284, 162, 83, '', 4, '', 1, 1250, 4),
(285, 162, 91, '', 6, '', 1, 2000, 6),
(286, 163, 83, '', 4, '', 1, 1250, 4),
(287, 163, 91, '', 3, '', 1, 2000, 3),
(288, 164, 90, '', 7, '', 1, 3000, 7),
(289, 165, 85, '', 7, '', 1, 150, 7),
(290, 166, 83, '', 4, '', 1, 1250, 4),
(291, 167, 83, '', 3, '', 1, 1250, 3),
(292, 167, 84, '', 2, '', 1, 2000, 2),
(293, 167, 87, '', 1, '', 15, 2500, 1),
(294, 167, 88, '', 1, '', 1, 1500, 1),
(295, 167, 90, '', 1, '', 1, 3000, 1),
(296, 167, 91, '', 1, '', 1, 2000, 1),
(297, 168, 85, '', 1, '', 1, 150, 1),
(298, 168, 86, '', 1, '', 1, 300, 1),
(299, 169, 85, '', 2, '', 1, 150, 2),
(300, 170, 85, '', 8, '', 1, 150, 8),
(301, 171, 85, '', 1, '', 1, 150, 1),
(302, 171, 86, '', 2, '', 1, 300, 2),
(303, 171, 93, '', 2, '', 1, 1230, 2),
(304, 172, 85, '', 3, '', 1, 150, 3),
(305, 173, 90, '', 3, '', 1, 3000, 3),
(306, 173, 91, '', 2, '', 1, 2000, 2),
(307, 173, 83, '', 2, '', 1, 1250, 2),
(308, 173, 92, '', 1, '', 0, 2400, 1),
(309, 174, 85, '', 3, '', 1, 150, 3),
(310, 174, 86, '', 1, '', 1, 300, 1),
(311, 174, 93, '', 2, '', 1, 1230, 2),
(312, 175, 85, '', 4, '', 1, 150, 4),
(313, 176, 85, '', 5, '', 1, 150, 5),
(314, 177, 85, '', 1, '', 1, 150, 1),
(315, 177, 86, '', 1, '', 1, 300, 1),
(316, 178, 91, '', 1, '', 1, 2000, 1),
(317, 178, 92, '', 1, '', 0, 2400, 1),
(318, 178, 90, '', 1, '', 1, 3000, 1),
(319, 179, 97, '', 1, '', 50, 630, 1),
(320, 180, 97, '', 1, '', 50, 630, 1),
(321, 180, 92, '', 2, '', 0, 2400, 2),
(322, 181, 89, '', 2, '', 0, 1630, 2),
(323, 182, 84, '', 1, '', 1, 1260, 1),
(324, 182, 87, '', 1, '', 15, 1360, 1),
(325, 182, 88, '', 2, '', 1, 1500, 2),
(326, 183, 100, '', 2, '', 50, 1230, 2),
(327, 184, 90, '', 1, '', 1, 2740, 1),
(328, 185, 85, '', 1, '', 1, 150, 1),
(329, 185, 93, '', 2, '', 1, 1230, 2),
(330, 186, 86, '', 3, '', 1, 300, 3),
(331, 187, 95, '', 2, '', 50, 550, 2),
(332, 188, 97, '', 1, '', 50, 630, 1),
(333, 189, 92, '', 1, '', 0, 2400, 1),
(334, 189, 107, '', 3, '', 50, 280, 3),
(335, 190, 109, '', 2, '', 50, 280, 2),
(336, 190, 92, '', 1, '', 0, 2400, 1),
(337, 190, 110, '', 1, '', 50, 280, 1),
(338, 190, 86, '', 2, '', 1, 300, 2),
(339, 191, 92, '', 1, '', 0, 2400, 1),
(340, 192, 84, '', 1, '', 1, 1260, 1),
(341, 193, 86, '', 1, '', 1, 300, 1),
(342, 193, 95, '', 1, '', 50, 550, 1),
(343, 193, 83, '', 1, '', 1, 1250, 1),
(344, 193, 84, '', 1, '', 1, 1260, 1),
(345, 194, 95, '', 2, '', 50, 550, 2),
(346, 194, 100, '', 1, '', 50, 1230, 1),
(347, 195, 94, '', 1, '', 1, 500, 1),
(348, 196, 86, '', 5, '', 1, 300, 5),
(349, 196, 94, '', 4, '', 1, 500, 4),
(350, 196, 108, '', 3, '', 50, 240, 3),
(351, 197, 86, '', 3, '', 1, 300, 3),
(352, 197, 90, '', 2, '', 1, 2740, 2),
(353, 198, 83, '', 3, '', 1, 1250, 3),
(354, 198, 90, '', 3, '', 1, 2740, 3),
(355, 198, 91, '', 2, '', 1, 2020, 2),
(356, 198, 92, '', 3, '', 0, 2400, 3),
(357, 198, 97, '', 2, '', 50, 630, 2),
(358, 198, 108, '', 1, '', 50, 240, 1),
(359, 199, 105, '', 7, '', 50, 520, 7),
(360, 200, 86, '', 1, '', 1, 300, 1),
(361, 200, 93, '', 1, '', 1, 1230, 1),
(362, 200, 94, '', 1, '', 1, 500, 1),
(363, 201, 83, '', 1, '', 1, 1250, 1),
(364, 202, 95, '', 2, '', 50, 550, 2),
(365, 202, 93, '', 1, '', 1, 1230, 1),
(366, 202, 99, '', 1, '', 50, 980, 1),
(367, 202, 102, '', 2, '', 50, 1240, 2),
(368, 203, 95, '', 1, '', 50, 550, 1),
(369, 203, 86, '', 1, '', 1, 300, 1),
(370, 204, 86, '', 1, '', 1, 300, 1),
(371, 204, 95, '', 1, '', 50, 550, 1),
(372, 204, 96, '', 1, '', 50, 880, 1),
(373, 205, 83, '', 1, '', 1, 1250, 1),
(374, 205, 93, '', 1, '', 1, 1230, 1),
(375, 205, 94, '', 1, '', 1, 500, 1),
(376, 205, 91, '', 1, '', 1, 2020, 1),
(377, 205, 100, '', 1, '', 50, 1230, 1),
(378, 206, 86, '', 2, '', 1, 300, 2),
(379, 207, 89, '', 1, '', 0, 1630, 1),
(380, 207, 109, '', 2, '', 50, 280, 2),
(381, 208, 86, '', 2, '', 1, 300, 2),
(382, 208, 93, '', 1, '', 1, 1230, 1),
(383, 208, 84, '', 1, '', 1, 1260, 1),
(384, 209, 86, '', 1, '', 1, 300, 1),
(385, 209, 106, '', 1, '', 50, 120, 1),
(386, 210, 95, '', 1, '', 50, 550, 1),
(387, 211, 83, '', 1, '', 1, 1250, 1),
(388, 211, 84, '', 1, '', 1, 1260, 1),
(389, 212, 95, '', 1, '', 50, 550, 1),
(390, 213, 86, '', 1, '', 1, 300, 1),
(391, 213, 93, '', 1, '', 1, 1230, 1),
(392, 213, 94, '', 1, '', 1, 500, 1),
(393, 213, 105, '', 1, '', 50, 520, 1),
(394, 214, 95, '', 1, '', 50, 550, 1),
(395, 215, 105, '', 1, '', 50, 520, 1),
(396, 216, 86, '', 1, '', 1, 300, 1),
(397, 217, 94, '', 2, '', 1, 500, 2),
(398, 217, 110, '', 1, '', 50, 280, 1),
(399, 218, 86, '', 1, '', 1, 300, 1),
(400, 218, 106, '', 5, '', 50, 120, 5),
(401, 219, 83, '', 1, '', 1, 1250, 1),
(402, 220, 107, '', 1, '', 50, 280, 1),
(403, 220, 83, '', 1, '', 1, 1250, 1),
(404, 221, 86, '', 1, '', 1, 300, 1),
(405, 222, 106, '', 1, '', 50, 120, 1),
(406, 222, 105, '', 2, '', 50, 520, 2),
(407, 222, 94, '', 1, '', 1, 500, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` varchar(200) NOT NULL,
  `title` varchar(100) NOT NULL,
  `value` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `value`) VALUES
('1', 'minmum order amount', '1'),
('2', 'maxmum order amount', '150000');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL,
  `slider_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `slider_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `slider_image` varchar(100) NOT NULL,
  `slider_status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `slider_title`, `slider_url`, `slider_image`, `slider_status`) VALUES
(1, 'あなただけのピザをご用意します。', '', 'main-02.jpg', 1),
(3, '手間ひまかけて作る自慢の特製ソースを使用したパスタ', '', 'main-01.jpg', 1),
(4, '豊富なドリンクメニューをご用意しています', '', 'main-03.jpg', 1),
(5, 'ワンランク上のサラダをご提供しております。', '', 'main-04.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `socity`
--

CREATE TABLE IF NOT EXISTS `socity` (
  `socity_id` int(11) NOT NULL,
  `socity_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pincode` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `delivery_charge` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=943 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `socity`
--

INSERT INTO `socity` (`socity_id`, `socity_name`, `pincode`, `delivery_charge`) VALUES
(939, '東京都港区', '369851', 740),
(940, '東京都新宿区', '', 320),
(941, '東京都渋谷区', '', 120);

-- --------------------------------------------------------

--
-- Table structure for table `time_slots`
--

CREATE TABLE IF NOT EXISTS `time_slots` (
  `opening_time` time NOT NULL,
  `closing_time` time NOT NULL,
  `time_slot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time_slots`
--

INSERT INTO `time_slots` (`opening_time`, `closing_time`, `time_slot`) VALUES
('09:00:00', '21:00:00', 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_email` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_phone` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_fullname` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_password` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `user_bdate` date NOT NULL,
  `is_email_varified` int(11) NOT NULL,
  `varified_token` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_gcm_code` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_ios_token` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_status` int(11) NOT NULL,
  `user_image` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_city` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_phone`, `user_fullname`, `user_password`, `user_type_id`, `user_bdate`, `is_email_varified`, `varified_token`, `user_gcm_code`, `user_ios_token`, `user_status`, `user_image`, `user_city`) VALUES
(51, '', 'gopupatel@gmail.com', '', 'gopupatel', 'ede997b0caf2ec398110d79d9eba38bb', 1, '0000-00-00', 0, '', '', '', 1, '', 0),
(52, 'admintest', 'admintest@delivery.com', '081122334455', 'Admin Test', '30bd40b8c6d89b56391a35275fbcc228', 0, '1993-02-25', 1, '', '', '', 1, '', 0),
(53, 'kawaguchi', 'kawaguchi@galilei.ne.jp', '081245454555', 'Kawaguchi', '5f1c591f12ecc1377b72750c2f2c254f', 0, '2018-09-11', 1, '', '', '', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_location`
--

CREATE TABLE IF NOT EXISTS `user_location` (
  `location_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pincode` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `socity_id` int(11) NOT NULL,
  `house_no` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `receiver_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `receiver_mobile` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_location`
--

INSERT INTO `user_location` (`location_id`, `user_id`, `pincode`, `socity_id`, `house_no`, `receiver_name`, `receiver_mobile`) VALUES
(15, 125, '363641', 935, '25 syam park', 'subhash sanghani', '9979038886'),
(16, 124, '363625', 935, '1 una', 'rajesh', '8530875124'),
(17, 124, '363625', 934, '21 una', 'rajesh', '8530875124'),
(18, 125, '363625', 935, '301 syam park', 'suresh kotadiya', '9979088888'),
(19, 126, '363625', 935, 'shreehari web Street', 'shreehari', '9974850403'),
(20, 127, '363625', 934, '34 shaym tower', 'satish', '9658741238'),
(21, 128, '363625', 934, '23,shreehari Street', 'subhash', '9632587410'),
(22, 129, '363625', 935, '23 floor plan and', 'rajesh', '9852346177'),
(23, 130, '363625', 935, '23, shreehari tower', 'user name', '9632580741'),
(24, 130, '15782', 938, 'jdhd', 'punita', '654123987056'),
(25, 133, '', 935, 'JL. Coba Coba Coba', 'Budi Budiman', '08111111112323'),
(31, 133, '', 939, 'JL. Batusari no 3', 'Ali’s Home', '08125456'),
(32, 133, '', 936, 'JL. Mantap bgt', 'LULU', '08156156'),
(33, 136, '', 934, 'test', 'やまだ', '0361238091'),
(34, 137, '', 938, 'Hdudhdudhdudhooopp', 'Yuu', '0814242535367'),
(35, 139, '', 936, 'Huji yuuk st', 'Home', '081990879866'),
(36, 141, '', 940, 'jalan gunung mandalawangi no 2A Denpasar Barat', 'dayu ratna ', '08563802498'),
(37, 141, '', 940, 'Jln Gunung Mandalawangi no 2A Denpasar Barat', 'Jalan Gunung Mandalawangi No 2A Denpasar Barat', '08563802498'),
(38, 140, '', 940, 'Jalam batusari', 'Gunung Agung', '085123456789'),
(39, 135, '', 941, 'Mantapppu', 'Yuuyu', '081354677897'),
(40, 144, '', 940, '新宿2-33-3 マンションアプキー303号室', '中村淳之介', '07034154138'),
(41, 133, '', 940, 'JL. Batusari 3 No 3 Dangin puri kelod, denpasar timur, Denpasar, Bali, Indonesia, Indonesia Raya, Asia Tenggara, Bumi', 'My Home', '058845654654'),
(42, 138, '', 940, 'つ子は。悲惨なのでが淳之介ちゃん　悲惨が　ウイルス性胃腸炎　アドセンス　鈴木', 'Appkey', '08135346461313'),
(43, 145, '', 941, '代々木3-1-11', 'テスト', '08088589158'),
(44, 145, '', 939, '芝浦1-1-1', 'テスト2', '1234567890'),
(45, 146, '', 939, 'Jalan batusari renon', 'Dadi', '089644282085'),
(46, 147, '', 939, '新宿2-33-3マンションアプキー303号室', '中村太郎', '07034154139'),
(47, 134, '', 939, 'aaa', 'aaa', '08980782257'),
(48, 149, '', 939, '赤坂1-1-17-903', 'スマートルーティン株式会社', '0488140666'),
(49, 140, '', 940, 'Dd', 'Q', '552'),
(51, 150, '', 940, 'Batu sari', 'Appkey', '08980782257'),
(52, 149, '', 941, 'やはは「わはまはみ', 'あのまやひ', '07015666666');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `user_type_id` int(11) NOT NULL,
  `user_type_title` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`user_type_id`, `user_type_title`) VALUES
(1, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `user_type_access`
--

CREATE TABLE IF NOT EXISTS `user_type_access` (
  `user_type_id` int(11) NOT NULL,
  `class` varchar(30) NOT NULL,
  `method` varchar(30) NOT NULL,
  `access` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type_access`
--

INSERT INTO `user_type_access` (`user_type_id`, `class`, `method`, `access`) VALUES
(0, 'admin', '*', 1),
(0, 'child', '*', 1),
(0, 'parents', '*', 1),
(0, 'timeline', '*', 1),
(0, 'users', '*', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `closing_hours`
--
ALTER TABLE `closing_hours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pageapp`
--
ALTER TABLE `pageapp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`purchase_id`);

--
-- Indexes for table `registers`
--
ALTER TABLE `registers`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- Indexes for table `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`sale_id`);

--
-- Indexes for table `sale_items`
--
ALTER TABLE `sale_items`
  ADD PRIMARY KEY (`sale_item_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socity`
--
ALTER TABLE `socity`
  ADD PRIMARY KEY (`socity_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- Indexes for table `user_location`
--
ALTER TABLE `user_location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`user_type_id`);

--
-- Indexes for table `user_type_access`
--
ALTER TABLE `user_type_access`
  ADD UNIQUE KEY `user_type_id` (`user_type_id`,`class`,`method`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `closing_hours`
--
ALTER TABLE `closing_hours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `pageapp`
--
ALTER TABLE `pageapp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `purchase_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `registers`
--
ALTER TABLE `registers`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `sale`
--
ALTER TABLE `sale`
  MODIFY `sale_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=223;
--
-- AUTO_INCREMENT for table `sale_items`
--
ALTER TABLE `sale_items`
  MODIFY `sale_item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=408;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `socity`
--
ALTER TABLE `socity`
  MODIFY `socity_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=943;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `user_location`
--
ALTER TABLE `user_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
